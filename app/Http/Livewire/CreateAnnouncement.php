<?php

namespace App\Http\Livewire;

use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use App\Jobs\RemoveFaces;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateAnnouncement extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $price;
    public $category;
    public $temporary_images;
    public $images = [];
    public $image;
    public $form_id;
    public $announcement;


    protected $rules=[
        'title'=>'required|min:3',
        'body'=>'required|min:10',
        'category'=>'required',
        'price'=>'required|numeric',
        'images.*'=>'image|max:1024',
        'temporary_images.*'=>'image|max:1024',

    ];

    protected $messages = [
        'title.required'=>"Il titolo è necessario.",
        'title.min'=>"Il titolo dev'essere di almeno tre caratteri.",
        'body.required'=>"Aggiungi una descrizione all'articolo.",
        'body.min'=>"La descrizione dev'essere chiara ed intuitiva.",
        'price.required'=>"Il prezzo è necessario.",
        'price.numeric'=>"Il prezzo va espresso in numeri.",
        'category.required'=>"Inserisci almeno una categoria.",
        'temporary_images.required' =>"l'immagine è necessaria",
        'temporary_images.*.image' =>"I file devono essere delle immagini",
        'temporary_images.*.max' =>"l'immagine dev'essere massimo di 1mb",
        'images.image' =>"l'immagine dev'essere un'immagine",
        'images.max' =>"l'immagine dev'essere massimo 1mb",


    ];

    public function updatedTemporaryImages(){
        if($this->validate([
            'temporary_images.*'=>'image|max:1024',
        ])) {
            foreach($this->temporary_images as $image){
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key){
         if(in_array($key, array_keys($this->images))){
             unset($this->images[$key]);
         } 
    }

    public function store(){

        $this->validate();
        
        $this->announcement=Category::find($this->category)->announcements()->create([
            'title'=>$this->title,
            'body'=>$this->body,
            'price'=>$this->price,
        ]);
        if(count($this->images)){
            foreach($this->images as $image){
                /* $this->announcement->images()->create(['path'=>$image->store('images','public')]); */
                $this->announcement->user()->associate(Auth::user());
                $this->announcement->save();
                $newFileName="announcements/{$this->announcement->id}";
                $newImage = $this->announcement->images()->create(['path'=>$image->store($newFileName,'public')]);

                RemoveFaces::withChain([
                    
                    new ResizeImage($newImage->path , 250 ,250),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),
                ])->dispatch($newImage->id);

            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));

            
        }

        
        session()->flash('message', 'Annuncio inserito con successo,sarà pubblicato dopo la revisione');
        $this->cleanForm();

    }

    public function updated($propertyName){

        $this->validateOnly($propertyName);

    }

    public function cleanForm(){
        $this->title="";
        $this->body="";
        $this->price="";
        $this->category="";
        $this->image="";
        $this->images= [];
        $this->temporary_images=[];
        $this->form_id = rand();


    }

    public function render()
    {
        return view('livewire.create-announcement');
    }
}




