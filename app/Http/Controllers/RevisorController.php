<?php

namespace App\Http\Controllers;

use App\Mail\BecomeRevisor;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index(){
        $announcement_to_check= Announcement::where('is_accepted' , null)->first();
        return view('revisor.index' , compact('announcement_to_check'));
    }

    public function acceptAnnouncement(Announcement $announcement){
        $announcement->setAccepted(true);
        return redirect()->back()->with('message' , "Complimenti, hai accettato l'annuncio");
    }

    public function rejectAnnouncement(Announcement $announcement){
        $announcement->setAccepted(false);
        return redirect()->back()->with('message' , "Hai rifiutato l'annuncio");  
    }

    public function becomeRevisor(Request $request){

        $message = $request->input('message');
        
        $user_contact = compact('message');

        Mail::to('admin@presto.it')->send(new BecomeRevisor(Auth::user() , $user_contact));
        return redirect()->back()->with('message','Complimenti! La tua richiesta di diventare Revisore è andata a buon fine!');
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:makeUserRevisor', ["email"=>$user->email]);
        return redirect()->back()->with('message','Complimenti sei ufficialmente un Revisore!');
    }


    public function updateRevisor(){
		$accepted_announcement=Announcement::where("is_accepted", true)->get()->sortByDesc('created_at')->take(10);
		$rejected_announcement=Announcement::where("is_accepted", false)->get()->sortByDesc('created_at')->take(10);
		return view('revisor.update', compact('accepted_announcement', 'rejected_announcement'));
	}

    public function nullRevisor(Announcement $announcement){
		$announcement->setAccepted(null);
		
		return redirect()->back()->with("message", "Dai un'altra occhiata alla revisione, è nuovamente disponibile nell'area revisore!");
	}




}
