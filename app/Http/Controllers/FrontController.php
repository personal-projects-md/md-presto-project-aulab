<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    // public function listAnnouncement(){

        // $announcements = Announcement::where('is_accepted', true)->take(4)->get()->sortByDesc('created_at');

    //     return view('announcements.show' , compact('announcements'));
    // }

    public function welcome(){
        $announcements = Announcement::orderBY('created_at', 'desc')->take(4)->get();
        return view('welcome', compact('announcements'));
    }

    public function categoryShow(Category $category){

        return view('categoryShow' , compact('category'));
    }

    public function searchAnnouncements(Request $request){
        $announcements = Announcement::search($request->searched)->where('is_accepted', true)->paginate(10);
        return view('announcements.index', compact('announcements'));
    }

    public function setLanguage($lang){

        session()->put('locale', $lang);
        return redirect()->back();
    }

}
