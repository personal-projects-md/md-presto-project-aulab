<x-layout>
    <x-slot name="title">{{__('indexAnnouncement.phrase1')}}</x-slot>
    
    <h1 class="display1 text-yellow headershadow text-center textmarginlog art-title">{{__('indexAnnouncement.phrase2')}}</h1>
    <h4 class="text-blue headershadow text-center my-3">{{__('indexAnnouncement.phrase3')}}</h4>
    
    <div class="container-fluid">
        <div class="row">
            
             <!-- SIDEBAR MENU UTENTE RICERCA ARTICOLI BROWSER  -->
            <div class="col-12 col-md-3 col-xl-2 px-sm-2 px-0 colshadow phonehide">
                <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                    
                    <h5 class="text-blue text-center mt-3">{{__('indexAnnouncement.phrase6')}}</h5>
                    
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                        <li>
                            <form action="{{route('announcements.search')}}" method="get" class="d-flex">
                                <input name="searched" class="form-control me-2 mt-3 mb-4" placeholder=" {{__('indexAnnouncement.phrase15')}} " aria-label="search" type="search">
                            </form>
                        </li>
                        <li><a class="dropdown-item my-4" href="{{route('announcements.create')}}">  {{__('indexAnnouncement.phrase7')}} <i class="ms-1 fas fa-bullhorn text-yellow"></i></a> </li>
                        <li>
                            <a href="#submenu1" data-bs-toggle="collapse" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline dropdown-toggle artcustomlabel">{{__('indexAnnouncement.phrase8')}}</span> </a>
                                <ul class="collapse show nav flex-column ms-1" id="submenu1" data-bs-parent="#menu">
                                    
                                    @foreach ($categories as $category)
                                    <li>
                                        @if(session('locale')=='it')
                                        <a href="{{route('categoryShow' , compact('category'))}}" class="dropdown-item ">{{($category->name_it)}}</a>
                                        @elseif(session('locale')=='en')
                                        <a href="{{route('categoryShow' , compact('category'))}}" class="dropdown-item ">{{($category->name_en)}}</a>
                                        @elseif(session('locale')=='es')
                                        <a href="{{route('categoryShow' , compact('category'))}}" class="dropdown-item ">{{($category->name_es)}}</a>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            
                            <li>
                        </ul>
                    </div>
                </div>
                
                <!--  SIDEBAR MENU UTENTE RICERCA ARTICOLI PHONE -->
                
                
                <div class="col-12 phoneshow mt-5">
                    
                    
                    <div class="accordion colshadow" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h5 class="text-blue text-center mt-3"> {{__('indexAnnouncement.phrase9')}} <i class="text-yellow mx-1"> {{__('indexAnnouncement.phrase10')}} </i> {{__('indexAnnouncement.phrase11')}}</h5>
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    
                                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start" id="menu">
                                        <li>
                                            <form action="{{route('announcements.search')}}" method="get" class="d-flex">
                                                <input name="searched" class="form-control me-2 mt-2" placeholder=" {{__('indexAnnouncement.phrase15')}} " aria-label="search" type="search">
                                            </form>
                                        </li>
                                        <li><a class="dropdown-item my-5 " href="{{route('announcements.create')}}">  {{__('indexAnnouncement.phrase7')}} <i class="ms-1 fas fa-bullhorn text-yellow"></i></a> </li>
                                        <li>
                                            <label for="category" class="form-label artcustomlabelphone dropdown dropdown-toggle btn" data-bs-toggle="dropdown" aria-expanded="false">{{__('indexAnnouncement.phrase8')}}</label>
                                            <ul class="dropdown-menu" aria-labelledby="categoriesDropdown">
                                                @foreach ($categories as $category)
                                                <li>
                                                    @if(session('locale')=='it')
                                                    <a href="{{route('categoryShow' , compact('category'))}}" class="dropdown-item ">{{($category->name_it)}}</a>
                                                    @elseif(session('locale')=='en')
                                                    <a href="{{route('categoryShow' , compact('category'))}}" class="dropdown-item ">{{($category->name_en)}}</a>
                                                    @elseif(session('locale')=='es')
                                                    <a href="{{route('categoryShow' , compact('category'))}}" class="dropdown-item ">{{($category->name_es)}}</a>
                                                    @endif
                                                </li>
                                                <li class="dropdown-divider"></li>
                                                @endforeach
                                            </ul>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        
                        
                        <!--  CARD ARTICOLI  -->
                        @foreach ($announcements as $announcement)
                        @if($announcement->is_accepted == true) 
                        
                        <div class="col-12 col-md-3 cardmargin my-5">
                            <div class="section text-center py-5 py-md-0">
                                <div class="card-3d-wrap mx-auto">
                                    <div class="card-3d-wrapper">

                                        <div class="card-front">
                                            <div class="pricing-wrap">
                                                <h4 class="mt-5 text-blue">{{$announcement->title}}</h4>
                                                @if(session('locale')=='it')
                                                <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                    {{($announcement->category->name_it)}}
                                                </a>
                                                @elseif(session('locale')=='en')
                                                <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                    {{($announcement->category->name_en)}}
                                                </a>
                                                @elseif(session('locale')=='es')
                                                <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                    {{($announcement->category->name_es)}}
                                                </a>
                                                @endif
                                        
                                        </a>

                                                <h2 class="my-4 text-blue">{{$announcement->price}}<sup>{{__('card.cardprice')}}</sup></h2>
                                        
                                                <p class="my-2 text-blue small mt-5"><i class="fas fa-clock text-blue small"></i>
                                                {{__('card.cardUpdated')}} {{$announcement->created_at->format('d/m/Y')}}</p>
                                                <p class="my-2 text-blue small"><i class="fas fa-user-circle text-blue small"></i></i>{{__('card.cardAuthor')}} {{$announcement->user->name ?? ''}}</p>
                                                <a href="{{route('announcements.detail', compact('announcement'))}}" class="btn btn-custom2">{{__('card.cardButton')}}</a>
                                                
                                                <div class="img-wrap img-2">
                                                    <img src="/media/cardbg.png" alt="">
                                                </div>
                                                <!--Swiper Immagini Annuncio-->
                                                <div class="carousel slide carousel-fade" data-bs-ride="carousel" id="showCarousel">
                                                    @if(count($announcement->images) > 0)
                                                        @foreach($announcement->images as $image)
                                                    
                                                        <div class="carousel-item @if($loop->first)active @endif">
                                                        <img src="{{$image->getUrl(250,250)}}" alt="#" class="img-wrap img-1">
                                                        </div>
                                                        @endforeach 
                                                    @else
                                                        <div class="carousel-item">
                                                        <img src="/media/noimg.png" alt="#" class="swiper-slide">
                                                      </div>
                                                    @endif
                                                </div> 
                                            </div>
                                            <!--End Swiper Immagini Annuncio-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
                
                
                
                
            </div>
            <div class="container-fluid p-5">
                <div class=" row justify-content-center ">
                    <div class=" col-12 col-md-1 ">
                        {{$announcements->links()}}
                    </div>
                </div>
            </div>
            
        </x-layout>