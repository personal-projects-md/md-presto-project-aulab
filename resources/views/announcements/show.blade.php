<x-layout>
    <x-slot name="title">Presto.it - Newest</x-slot>

<h1>{{__('announcementShow.showText0')}}</h1>

<div class="container">
    <div class="row">
        @foreach ($announcements as $announcement)
        @if($announcement->is_accepted == true) 
        
        <div class="col-12 col-md-3 cardmargin my-5">
            <div class="section text-center py-5 py-md-0">
                <div class="card-3d-wrap mx-auto">
                    <div class="card-3d-wrapper">
                        <div class="card-front">
                            <div class="pricing-wrap">
                                
                                <h4 class="mt-5 text-blue">{{$announcement->title}}</h4>
                                <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                    @if(session('locale')=='it')
                                    <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                        {{($announcement->category->name_it)}}
                                    </a>
                                    @elseif(session('locale')=='en')
                                    <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                        {{($announcement->category->name_en)}}
                                    </a>
                                    @elseif(session('locale')=='es')
                                    <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                        {{($announcement->category->name_es)}}
                                    </a>
                                    @endif
                                </a>
                                
                                <h2 class="my-4 text-blue">{{$announcement->price}}<sup>{{__('card.cardprice')}}</sup></h2>
                                
                                <p class="my-2 text-blue small mt-5"><i class="fas fa-clock text-blue small"></i>{{__('card.cardUpdated')}} {{$announcement->created_at->format('d/m/Y')}}</p>
                                <p class="my-2 text-blue small"><i class="fas fa-user-circle text-blue small"></i></i>{{__('card.cardAuthor')}} {{$announcement->user->name ?? ''}}</p>
                                <a href="{{route('announcements.detail', compact('announcement'))}}" class="link btn btn-custom2">{{__('card.cardButton')}}</a>
                                
                                <div class="img-wrap img-2">
                                    <img src="/media/cardbg.png" alt="">
                                </div>
                                <!--Swiper Immagini Annuncio-->

        <div class="card" style="width: 18rem;">
            <img src="https://picsum.photos/300" class="card-img-top" alt="immagine dell'annuncio">
            <div class="card-body">
            <h5 class="card-title">{{$announcement->title}}</h5>
           <!--  <p class="card-text">{{$announcement->body}}</p> -->
            <p class="card-text">{{$announcement->price}}</p>
            <a href="{{route('announcements.detail', compact('announcement'))}}" class="btn btn-primary">{{__('announcementShow.showText1')}}</a>
            <a href="" class="btn btn-primary my-3">{{__('announcementShow.showText2')}} {{$announcement->category->name}}</a>
            <p class="card-footer">{{__('announcementShow.showText3')}} {{$announcement->created_at->format('d/m/Y')}}</p>
            
        </div>
        </div>
        </div>
        @endif
        @endforeach
    </div>
</div>


</x-layout>