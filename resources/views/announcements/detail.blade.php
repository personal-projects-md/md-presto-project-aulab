
<x-layout>

    <x-slot name="title">{{__('detailAnnouncement.phrase1')}}</x-slot>

        <div class="section over-hide">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    
                    <div class="col-12 col-md-4 cardmargin my-5">
                        <div class="section text-center py-5 py-md-0">
                            <div class="card-3d-wrap mx-auto">
                                <div class="card-3d-wrapper">
                                    <div class="card-front">
                                        <div class="pricing-wrap">
                                            
                                            <h4 class="mt-5 text-blue">{{$announcement->title}}</h4>
                                            @if(session('locale')=='it')
                                            <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                {{($announcement->category->name_it)}}
                                            </a>
                                            @elseif(session('locale')=='en')
                                            <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                {{($announcement->category->name_en)}}
                                            </a>
                                            @elseif(session('locale')=='es')
                                            <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                {{($announcement->category->name_es)}}
                                            </a>
                                            @endif

                                            <h2 class="my-4 text-blue">{{$announcement->price}}<sup>{{__('card.cardprice')}}</sup></h2>
                                        
                                        <p class="my-2 text-blue small mt-5"><i class="fas fa-clock text-blue small"></i>
                                        {{__('card.cardUpdated')}} {{$announcement->created_at->format('d/m/Y')}}</p>
                                        <p class="my-2 text-blue small"><i class="fas fa-user-circle text-blue small"></i></i>{{__('card.cardAuthor')}} {{$announcement->user->name ?? ''}}</p>


                                            <div class="img-wrap img-2">
                                                <img src="/media/cardbg.png" alt="">
                                            </div>
                                            <!--Swiper Immagini Annuncio-->
    

                                              <div class="carousel slide carousel-fade" data-bs-ride="carousel" id="showCarousel">
                                            @if(count($announcement->images) > 0)
                                                @foreach($announcement->images as $image)
                                            
                                                <div class="carousel-item @if($loop->first)active @endif">
                                                <img src="{{$image->getUrl(250,250)}}" alt="#" class="img-wrap img-1">
                                                </div>
                                                @endforeach 
                                            @else
                                                <div class="carousel-item">
                                                <img src="/public/media/noimg.png" alt="#" class="img-wrap img-1">
                                              </div>
                                            @endif
                                        </div> 


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-8 text-center mb-5">
                        <h1 class="text-cyan"> {{__('detailAnnouncement.phrase4')}} </h1>
                        <h3 class="mt-5 text-blue">{{$announcement->body}}</h3>
                        <a href="{{route('announcements.index')}}" class="btn btn-custom2"> {{__('detailAnnouncement.phrase6')}} </a>

                    </div>
                </div>
            </div>
        </div>
        </div>

    </x-layout>