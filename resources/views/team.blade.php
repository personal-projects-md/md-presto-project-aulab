<x-layout>
    <x-slot name="title">Team</x-slot>
    
    <h2 class="display1 text-center text-yellow headershadow margincustomhome">Hey, <i class="text-cyan">you </i>!</h2>
    <h3 class="text-blue text-center headershadow">Take a look at our <i class="text-yellow mx-1">team</i></h3>


    <div class="container articleshadow justify-content-center my-5">
        <div class="row justify-content-center align-items-center text-center">
            <div class="col-12 col-md-5 my-5">
                <img src="/media/marco.png" class="img-thumbnail rounded-circle" style="width: 300px; height: 300px;" alt="">
             </div>
            <div class="col-12 col-md-6 my-5">
                <h1 class="text-cyan headershadow mt-5">Marco Donvito</h1>
                <h5 class="text-yellow headershadow mb-5">Junior Full-Stack Developer</h5>
                <h4 class="text-blue headershadow mb-5">{{__('team.marco')}}</h4>
            </div>
        </div>

        <div class="row justify-content-center align-items-center text-center">
            <div class="col-12 col-md-5 my-5">
                <img src="/media/antonio.png" class="img-thumbnail rounded-circle" style="width: 300px; height: 300px;" alt="">
            </div>
            <div class="col-12 col-md-6 my-5">
                <h1 class="text-cyan headershadow mt-5">Antonio Junior Paparella</h1>
                <h5 class="text-yellow headershadow mb-5">Junior Full-Stack Developer</h5>
                <h4 class="text-blue headershadow mb-5">{{__('team.antonio')}}</h4>
            </div>
        </div>

        <div class="row justify-content-center align-items-center text-center">
            <div class="col-12 col-md-5 my-5">
                <img src="/media/peter.png" class="img-thumbnail rounded-circle" style="width: 300px; height: 300px;" alt="">
            </div>
            <div class="col-12 col-md-6 my-5">
                  <h1 class="text-cyan headershadow mt-5">Peter Borgnetta</h1>
                  <h5 class="text-yellow headershadow mb-5">Junior Full-Stack Developer</h5>
                <h4 class="text-blue headershadow mb-5">{{__('team.peter')}}</h4>
            </div>
        </div>

        <div class="row justify-content-center align-items-center text-center">
            <div class="col-12 col-md-5 my-5">
                <img src="/media/andrea.png" class="img-thumbnail rounded-circle" style="width: 300px; height: 300px;" alt="">
            </div>
            <div class="col-12 col-md-6 my-5">
                  <h1 class="text-cyan headershadow mt-5">Andrea Tomasin</h1>
                  <h5 class="text-yellow headershadow mb-5">Junior Full-Stack Developer</h5>
                <h4 class="text-blue headershadow mb-5">{{__('team.andrea')}}</h4>
            </div>
        </div>

        <div class="row justify-content-center align-items-center text-center">
            <div class="col-12 col-md-5 my-5 mb-5">
                <img src="/media/matteo.jpeg" class="img-thumbnail rounded-circle" style="width: 300px; height: 300px;" alt="">
            </div>
            <div class="col-12 col-md-6 my-5">
                  <h1 class="text-cyan headershadow mt-5">Matteo Magaroli</h1>
                  <h5 class="text-yellow headershadow mb-5">Junior Full-Stack Developer</h5>
                <h4 class="text-blue headershadow mb-5">{{__('team.matteo')}}</h4>
            </div>
        </div>

    </div>
    
</x-layout>