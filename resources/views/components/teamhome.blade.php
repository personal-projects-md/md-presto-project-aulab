
<div class="container margincustomhome mb-5">
    
    <h3 class="display1 text-yellow headershadow text-center"> {{__('ui.ht')}}</h3>
    <h4 class="text-blue headershadow text-center"><i class="fas fa-users mx-5 text-yellow"></i>{{__('ui.ht1')}}<i class="fas fa-users mx-5 text-yellow"></i></h4>
    
    <div class="row min-vh-80 align-items-center">

        <div class="col-12 col-md-6">
            <img class="img-fluid" src="/media/teamhome.png" alt="Presto.it!" srcset="">
        </div>

        <div class="col-12 col-md-6 text-center">
             <h3 class="display1 text-yellow headershadow">{{__('ui.ht2')}}</h3>
             <h4 class="text-blue headershadow">{{__('ui.ht3')}} <i class="text-cyan mx-1">{{__('ui.ht4')}} </i>!</h4>
             <a href="{{route('team')}}" class="btn btn-custom my-2">{{__('ui.ht6')}}</a>
        </div>

    
    </div>
</div>
