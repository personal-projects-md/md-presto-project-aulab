  
  <nav class="navbar navbar-expand-lg navbar-light bg-ice sticky-top">

   

    <div>
      <a class="justify-content-start align-items-center" href="/"><img class="ms-3 img-fluid logo-custom" src="/media/logonavbar2.png" alt="Presto!" id="navlogo"></a>
    </div>

    

    <!--Dropdown Query-->
    <div class="phonedropdown">
      <a class="" href="/"></a>
      <i class="fas fa-caret-down text-blue fa-2x me-5 change" id="navlogo" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"></i>
    </div>

    <div class="container-fluid align-items-center justify-content-center phonedropdown">
      <div class="row text-center">
        <div class="col-4"><li class="list-unstyled"><x-_locale lang='it' nation='it'/></li></div>
        <div class="col-4"><li class="list-unstyled"><x-_locale lang='en' nation='gb'/></li></div>
        <div class="col-4"><li class="list-unstyled"><x-_locale lang='es' nation='es'/></li></div>
      </div>
    </div>
  

    <div class="container-fluid align-items-center justify-content-end querydrop">

          
    
      
      <div class="row">
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link text-bluenav  mx-2 @if(Route::currentRouteName() == 'welcome') activehome @endif" aria-current="page" href="/"><i class="fas fa-home-lg-alt text-blue"></i> Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link text-bluenav  mx-2 @if(Route::currentRouteName() == 'announcements.index') activehome @endif" href="{{route('announcements.index')}}"><i class="fas fa-tags text-darkMode"></i>{{__('navbar.language5')}}</a>
            </li>

            <!--Authorized User Categories-->
            @auth
            @if(Auth::user()->is_revisor)

            <li class="nav-item">
              <a class="mx-2 nav-link text-bluenav position-relative @if(Route::currentRouteName() == 'revisor.index') activehome @endif" aria-current="page" href="{{route('revisor.index')}}"><i class="fa-solid fa-clipboard-check"></i> {{__('navbar.language')}}
                <span class="position-absolute top-1 start-100 translate-middle badgerounded-pill text-danger icon-wrapper">
                <i class="far fa-bell bell-icon"></i>
                  {{App\Models\Announcement::toBeRevisionedCount()}}
              </span>
                  <span class="visually-hidden">unread messages</span>
                </a>

                


              </li>
              @endif
              @endauth
              
              <!--End Auth-->
              
              <!--User dropdown-->
              <li class="nav-item">
                <div class="dropdown mx-2">
                  <button class="btn dropdown-toggle text-bluenav2" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    @auth
                    <i class="fas fa-user text-blue"> </i> <span class="text-blue small ms-1">{{__('navbar.language1')}} {{Auth::user()->name}}</span>
                    @else
                    <i class="fas fa-user text-blue"> </i> <span class="text-blue small ms-1">{{__('navbar.language2')}}</span>
                    @endauth
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    @guest
                    <li><a class="dropdown-item" href="{{route('register')}}"><i class="fas fa-user-plus text-blue mx-2"></i>{{__('navbar.language3')}}</a></li>
                    <li class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{route('login')}}"><i class="fas fa-key text-blue mx-2"></i>{{__('navbar.language4')}}</a></li>
                  </ul>
                  @else
                  <ul class="list-unstyled p-2">
                    <li><a class="dropdown-item" href="/logout" onclick="event.preventDefault();getElementById('form-logout').submit() ; "><i class="fas fa-sign-out mx-2"></i>Logout</li></a>
                    <form id="form-logout" action="{{route('logout')}}" method="POST" class="d-none">@csrf</form>
                  </ul>
                  @endguest
                </div>
              </li>

              <li class="nav-item">

               
                <div class="dropdown phonehide">
                  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButtonlang" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fas fa-language text-blue" style="font-size: 22px;"></i>
                  </button>
                  <ul class="dropdown-menu langmenu" aria-labelledby="dropdownMenuButtonlang">
                    <li><x-_locale lang='it' nation='it'/></li>
                    <li><x-_locale lang='en' nation='gb'/></li>
                    <li><x-_locale lang='es' nation='es'/></li>
                  </ul>
                </div>

            </ul>
         
          </div>
        </div>
      </div>
    </nav>

  
    
    
    