 <footer class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <h3 class="text-yellow">PRESTO!</h3>
          <p class="text-justify text-ice">{{__('footer.ftText1')}}<i class="text-yellow">{{__('footer.ftTextH1')}}</i> <br> {{__('footer.ftT1s')}} <i class="text-yellow">{{__('footer.ftTextH2')}}</i> <br> {{__('footer.ftText1t')}}</p>
          <p class="text-justify text-ice">{{__('footer.ftText2')}} <i class="text-yellow">{{__('footer.ftTextH3')}}</i>!</p>
        </div>

        <div class="col-xs-6 col-md-3">
          <h6 class="txtfoot">FAQ</h6>
          <ul class="footer-links">
            <li><a class="afoot" href="{{route('terms&conditions')}}">{{__('footer.ftTerms')}}</a></li>
            <li><a class="afoot" href="{{route('terms&conditions')}}">Privacy Policy</a></li>
            <li><a class="afoot" href="{{route('terms&conditions')}}">Cookie Policy</a></li>
            <li><a class="afoot" href="https://wa.me/123456789" target="_blank">{{__('footer.ftHelp')}}</a></li>
          </ul>
        </div>

        <div class="col-xs-6 col-md-3">
          <h6 class="txtfoot">Presto! Links</h6>
          <ul class="footer-links">
            <li><a class="afoot" href="{{route('team')}}">{{__('footer.ftWho')}}</a></li>
            <li><a class="afoot" href="{{route('contacts')}}">{{__('footer.ftContact')}}</a></li>
            <li><a class="afoot" href="{{route('revisor.formRevisor')}}">{{__('footer.ftWork')}}</a></li>
          </ul>
        </div>
      </div>
      <hr>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-12">
          <p class="copyright-text">Copyright &copy; 2022 Backend-Street Boys -
       <a class="afoot" href="https://aulab.it/"> Aulab Hackademy46</a>
          </p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <ul class="social-icons">
            <li><a class="facebook" href="https://www.facebook.com/aulab/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/aulab_it" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a class="whatsapp" href="https://wa.me/123456789" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
            <li><a class="linkedin" href="https://www.linkedin.com/school/aulab-srl/" target="_blank"><i class="fa fa-linkedin"></i></a></li> 
            <li><a class="phone" href="#" aria-label="+39 123456789" data-balloon-pos="up"><i class="fa fa-phone"></i></a></li>    
          </ul>
        </div>
      </div>
    </div> 
</footer>
