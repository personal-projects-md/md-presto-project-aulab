<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/63523e72ab.js" crossorigin="anonymous"></script>
    
      <!--AOS CSS-->
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

      <!--SWIPER CSS-->
      <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
    
    @livewireStyles
    <!--CUSTOM CSS-->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    
    
    <title>{{$title ?? 'Presto.it'}}</title>
</head>
<body>

  <div class="wrapper">

    <x-navbar />
    
    {{$slot}}
    
    
    
    
    <x-footer />

  </div>

  {{-- <div class="switch">
    <img src="/media/sun.png" class="switch-sun" width="40" height="40">
    <img src="/media/moon.png" class="switch-moon" width="40" height="40">
  </div> --}}

    @livewireScripts
    
       <!--AoS JS-->
       <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

       <!--SWIPER JS-->
       <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
   
       <!--CUSTOM JS-->
       <script src="{{ asset('js/app.js') }}" defer></script>
   
       <script>
        var swiper = new Swiper(".mySwiper", {
          slidesPerView: 3,
          spaceBetween: 30,
          centeredSlides: true,
          pagination: {
            el: ".swiper-pagination",
            clickable: true,
          },
        });
      </script>

    <script>
        var swiper = new Swiper(".mySwiper", {
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>

    {{-- script darkMode --}}

    {{-- <script>
      const switchButton = document.querySelector('.switch');

      switchButton.addEventListener('click', () => {
      document.body.classList.toggle('dark-mode');
      });
    </script> --}}

    {{-- end script darkMode --}}
    
</body>
</html>