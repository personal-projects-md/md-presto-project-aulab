@guest
<div class="container">
    <div class="row min-vh-80 align-items-center  margincustomhome">
        <div class="col-12 col-md-6 text-center">
             <h3 class="display1 text-yellow headershadow">{{__('ui.allAnnouncements')}}</h3>
             <h4 class="text-blue headershadow">{{__('ui.allAnnouncements1')}} <i class="text-yellow mx-1">{{__('ui.allAnnouncements2')}} </i> {{__('ui.allAnnouncements3')}}</h4>
             <a href="{{route('register')}}" class="btn btn-custom my-2">{{__('navbar.language3')}}</a>
             <br>
             <a href="{{route('login')}}"><h6 class="text-grey small loghover">{{__('ui.allAnnouncements4')}}</h6></a>

        </div>

        <div class="col-12 col-md-6">
            <img class="img-fluid" src="/media/headerimg.png" alt="Presto.it!" srcset="">
        </div>

    </div>
</div>
@else 
<div class="container">
    <div class="row min-vh-80 align-items-center  margincustomhome">
        <div class="col-12 col-md-6 text-center">
             <h3 class="display1 text-yellow headershadow">{{__('ui.allAnnouncements5')}}, <i class="text-cyan">{{Auth::user()->name}}</i></h3>
             <h4 class="text-blue headershadow">{{__('ui.allAnnouncements6')}} <i class="text-yellow mx-1">{{__('ui.allAnnouncements7')}}</i> {{__('ui.allAnnouncements8')}}</h4>
             <a href="{{route('announcements.index')}}" class="btn btn-custom2 m-2">{{__('ui.allAnnouncements9')}}</a>
             <a href="{{route('announcements.create')}}" class="btn btn-custom2 m-2">{{__('ui.allAnnouncements10')}}</a>
        </div>

        <div class="col-12 col-md-6">
            <img class="img-fluid" src="/media/headerimg2.png" alt="Presto.it!" srcset="">
        </div>

    </div>
</div>
@endguest
