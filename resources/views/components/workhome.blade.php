
<div class="container margincustomhome mb-5">
    
    <h3 class="display1 text-yellow headershadow text-center"> {{__('ui.wt')}}</h3>
    <h4 class="text-blue headershadow text-center"><i class="fas mx-5 text-yellow fa-chevron-circle-down"></i>{{__('ui.wt1')}}<i class="fas mx-5 text-yellow fa-chevron-circle-down"></i></h4>
    
    <div class="row min-vh-80 align-items-center">

        

        <div class="col-12 col-md-6 text-center">
             <h3 class="display1 text-yellow headershadow">{{__('ui.wt2')}}</h3>
             <h4 class="text-blue headershadow">{{__('ui.wt3')}} <i class="text-cyan">{{__('ui.wt4')}} </i> {{__('ui.wt5')}}</h4>
             <a href="{{route('revisor.formRevisor')}}" class="btn btn-custom my-2">{{__('ui.wt6')}}</a>
        </div>

        <div class="col-12 col-md-6">
            <img class="img-fluid" src="/media/workhome.png" alt="Presto.it!" srcset="">
        </div>

    </div>
</div>
