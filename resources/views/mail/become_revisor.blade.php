<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Presto.it</title>
</head>
<body>

    <h1 style="font-size: 50px; color: #102770;">
        PRESTO.it REVISOR REQUEST
    </h1>

<div class="container" style="border-top: 20px solid #ffc845; border-bottom: 12px solid #102770; padding: 30px;">
    <div class="row justify-content-center align-items-center">

        <div class="col-12">
            <h1 style="font-size: 35px; color: #102770;">
                Un utente ha richiesto di lavorare con noi
            </h1>
        </div>

        <h2 style="font-size: 25px; color: #102770;">Di seguito i suoi dati</h2>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p style="font-size: 20px; color: #ffc845;">Nome: 
                
                        <i style="font-size: 15px; color: #3daeca;">
                         {{$user->name}}
                        </i>
                         
                     </p>
                     <p style="font-size: 20px; color: #ffc845;">Email: 
                         <i style="font-size: 15px; color: #3daeca;">
                             {{$user->email}}
                         </i>
                         
                     </p>
                </div>
            </div>
        </div>
           

           
            <p style="font-size: 20px; color: #102770;">Con il seguente messaggio: 
                <div class="container">
                    <div class="row text-center">
                        <div class="col-12" style="border-top: 20px solid #ffc845; border-bottom: 12px solid #102770; padding: 30px; border-radius: 10px;">
                        <p style="font-size: 25px; color: #3daeca;">
                            {{$user_contact['message']}}
                        </p>
                    </div>
                    </div>
                </div>
                
            </p>


            <p style="font-size: 30px; color: #ffc845;">Vuoi renderlo revisore clicca qui </p>
            <a href="{{route('make.revisor', compact('user'))}}" style="border: 2px solid #ffc845;
            color: white;
            background-color: #ffc845;
            width: 180px;
            padding: 15px;
            border-radius: 50px;">Conferma!</a>
        </div>
    </div>
</div>
    
</body>
<<<<<<< HEAD
</html> 
=======
</html>





>>>>>>> f542f3619c00bde643e732f41eb4f4c45e001e54
