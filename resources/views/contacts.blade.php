<x-layout>
    <x-slot name="title">Presto.it - Get in touch!</x-slot>
    
    <h2 class="display1 text-center text-yellow headershadow margincustomhome">{{__('ftlinks.contacts')}}</h2>

   <div class="container my-5">
       <div class="row text-center justify-content-center text-blue">
           <div class="col-6 col-md-6 articleshadow">
            <ul class="social-icons">
                <li><a class="facebook" href="https://www.facebook.com/aulab/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a class="twitter" href="https://twitter.com/aulab_it" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a class="whatsapp" href="https://wa.me/123456789" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                <li><a class="linkedin" href="https://www.linkedin.com/school/aulab-srl/" target="_blank"><i class="fa fa-linkedin"></i></a></li> 
                <li><a class="phone" href="#" aria-label="+39 123456789" data-balloon-pos="up"><i class="fa fa-phone"></i></a></li>    
              </ul>
           </div>
           <div class="col-12 margincustomhome">
            <a href="/" class="btn btn-custom2  my-5">Torna alla home</a>
           </div>
       </div>
   </div>


</x-layout>