<x-slot name="title">{{__('createAnnouncement.CreAnn0')}}</x-slot>

<div class="container">
  <div class="row justify-content-center text-center">
    
    
    
    <h1 class="display1 text-yellow headershadow text-center textmarginlog art-title">{{__('createAnnouncement.CreAnn1')}}</h1>
    <h4 class="text-blue headershadow text-center my-3">{{__('createAnnouncement.CreAnn2')}} <i class="text-yellow mx-1">{{__('createAnnouncement.CreAnn3')}}</i> {{__('createAnnouncement.CreAnn4')}}</h4>
    
    
    @if(session()->has('message'))
    <div class="flex flex-row justify-center my2 alert alert-success">
      {{session('message')}}
    </div>
    @endif
    
    <div class="col-12 my-5">
      <form wire:submit.prevent="store" class="justify-content-center articleshadow">
        @csrf
        <div class="row justify-content-center">
        <div class="col-6 mb-3">
          <label for="title" class="form-label artcustomlabel">{{__('createAnnouncement.CreAnn5')}}</label>
          <input wire:model.debounce.1500ms="title" type="text" class="form-control artcustomform @if(isset($title)): @error('title') is-invalid @else is-valid @enderror @endif">
          @error('title')
          <span class="fst-italic text-danger small">{{$message}}</span>
          @enderror
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-6 mb-3">
          <label for="body" class="form-label artcustomlabel">{{__('createAnnouncement.CreAnn6')}}</label>
          <textarea wire:model="body" type="text" class="form-control artcustomform @if(isset($body)): @error('body') is-invalid @else is-valid @enderror @endif"></textarea>
          @error('body')
          <span class="fst-italic text-danger small">{{$message}}</span>
          @enderror
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-6 mb-3">
          <label for="price" class="form-label artcustomlabel">{{__('createAnnouncement.CreAnn7')}}</label>
          <input wire:model="price" type="number" min="1" step="any" id="price" class="form-control artcustomform  @if(isset($price)): @error('price') is-invalid @else is-valid @enderror @endif">
          @error('price')
          <span class="fst-italic text-danger small">{{$message}}</span>
          @enderror
        </div>
      </div>
         <div class="row justify-content-center">
           <div class="col-6 mb-3">
            <label for="category" class="form-label artcustomlabel">{{__('createAnnouncement.CreAnn8')}}</label>
            <select wire:model.defer="category" id="category" class="form-control">
              <option value="">{{__('createAnnouncement.CreAnn9')}}</option>
              @foreach ($categories as $category)
              @if(session('locale')=='it')
              <option value="{{$category->id}}">{{$category->name_it}}</option>
              @elseif(session('locale')=='en')
              <option value="{{$category->id}}">{{$category->name_en}}</option>
              @elseif(session('locale')=='es')
              <option value="{{$category->id}}">{{$category->name_es}}</option>
              @endif
              @endforeach
            </select>   
          </div>
          </div>

        
        
        <div class="row justify-content-center">
    
        <div class="col-6 mb-3">
          <label for="category" class="form-label artcustomlabel">{{__('createAnnouncement.CreAnn10')}}</label>
          <input wire:model="temporary_images" type="file" name="images" multiple class="form-control  @error('temporary_images.*') is-invalid @enderror" placeholder="Img">
            <h6 class="text-blue headershadow text-center my-3">{{__('createAnnouncement.CreAnn11')}}<i class="text-yellow mx-1">{{__('createAnnouncement.CreAnn12')}}</i></h6>
          @error('temporary_images.*')
          <p class="mt-2">{{$message}}</p>
          @enderror
        </div>

      
      </div>
        @if(!empty($images))
        <div class="row">
          <div class="col-12">
            <h5 class="text-blue headershadow text-center my-3"><i class="text-yellow mx-1">{{__('createAnnouncement.CreAnn13')}}</i> {{__('createAnnouncement.CreAnn14')}}</h5>
            <h5 class="text-blue headershadow text-center my-3">{{__('createAnnouncement.CreAnn15')}}<i class="text-yellow mx-1">{{__('createAnnouncement.CreAnn16')}}</i> {{__('createAnnouncement.CraAnn17')}}</h5>
            <div class="row">
              @foreach($images as $key => $image)
              <div class="col-4 my-3">
                <div class="img-custom mx-auto rounded align-items-center justify-content-center" style="background-image: url({{$image->temporaryUrl()}}); background-repeat: no-repeat; background-position: center;  background-size: cover;"></div>
                <button type="button" class="btn btn-custom3 d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">{{__('createAnnouncement.annbtn')}}<i class="fa-solid fa-eraser mx-2"></i></button>
              </div>
                @endforeach
            </div>
          </div>
        </div>
        @endif
        <button type="submit" class="btn btn-customart m-2">{{__('createAnnouncement.CreAnn18')}}</button> 
      </div> 
      </form>
    </div>
  </div>
</div>