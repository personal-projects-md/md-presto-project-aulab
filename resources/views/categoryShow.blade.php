<x-layout>
    <x-slot name="title">{{__('categoryShow.CatShoT0')}}</x-slot>
    
    <h1 class="display1 text-yellow headershadow text-center textmarginlog art-title">{{__('categoryShow.CatShoT1')}}</h1>
    
    
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center text-center">
            
            
            @foreach ($category->announcements as $announcement)
            @if($announcement->is_accepted == true)

            <div class="col-12 col-md-3 cardmargin my-5">
                
                <!-- INIZIO CARD -->
                <div class="section text-center py-5 py-md-0">                                        
                    <div class="card-3d-wrap mx-auto">
                        <div class="card-3d-wrapper">
                            <div class="card-front">
                                <div class="pricing-wrap">
                                    <h4 class="mt-5 text-blue">{{$announcement->title}}</h4>
                                    @if(session('locale')=='it')
                                    <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                        {{($announcement->category->name_it)}}
                                    </a>
                                    @elseif(session('locale')=='en')
                                    <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                        {{($announcement->category->name_en)}}
                                    </a>
                                    @elseif(session('locale')=='es')
                                    <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                        {{($announcement->category->name_es)}}
                                    </a>
                                    @endif


                                    
                                    <h2 class="my-4">{{$announcement->price}}<sup>{{__('card.cardprice')}}</sup></h2>
                                    
                                    <p class="my-2 text-blue small mt-5"><i class="fas fa-clock text-blue small"></i>{{__('card.cardUpdated')}} {{$announcement->created_at->format('d/m/Y')}}</p>
                                    <p class="my-2 text-blue small"><i class="fas fa-user-circle text-blue small"></i></i>{{__('card.cardAuthor')}} {{$announcement->user->name ?? ''}}</p>
                                    <a href="{{route('announcements.detail', compact('announcement'))}}" class="btn btn-custom2">{{__('categoryShow.CatShoT9')}}</a>
                                    
                                    <div class="img-wrap img-2">
                                        <img src="/media/cardbg.png" alt="">
                                    </div>
                                    <!--Swiper Immagini Annuncio-->
                                    <div class="carousel slide carousel-fade" data-bs-ride="carousel" id="showCarousel">
                                        @if(count($announcement->images) > 0)
                                        @foreach($announcement->images as $image)
                                        
                                        <div class="carousel-item @if($loop->first)active @endif">
                                            <img src="{{$image->getUrl(250,250)}}" alt="#" class="img-wrap img-1">
                                        </div>
                                        @endforeach 
                                        @else
                                        <div class="carousel-item">
                                            <img src="/media/noimg.png" alt="#" class="swiper-slide">
                                        </div>
                                        @endif
                                    </div> 
                                </div>
                            </div> 
                        </div>                                           
                    </div>
                </div>
                <!-- FINE CARD -->
                
            </div>
            
            @endif
            @endforeach
            
            <div class="col-12 my-5">
                
                <h3 class="text-center text-blue headershadow my-4">{{__('categoryShow.CatShoT12')}}</h3>
                <a href="{{route('announcements.index')}}" class="btn btn-custom2 mx-2">{{__('categoryShow.CatShoT13')}}</a>
                
                <h1 class="display1 text-yellow headershadow text-center my-3 art-title">{{__('categoryShow.CatShoT10')}}<i class="fa-solid fa-face-frown text-blue"></i></h1>
                <h4 class="text-blue headershadow text-center my-3">{{__('categoryShow.CatShoT11')}} <i class=" mx-1 fa-solid fa-face-laugh-wink text-yellow"></i></h4>
                
                <a href="{{route('announcements.create')}}" class="btn btn-custom2 m-2">{{__('categoryShow.CatShoT4')}}</a>
  
            </div>    
        </div>
    </div>
</x-layout>




