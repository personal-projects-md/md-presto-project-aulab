<x-layout>
    
    
    <x-slot name="title">Presto.it - Join the team!</x-slot>

    
    <div class="container">
        <div class="row justify-content-center align-items-center text-center">
            
            
            
            <h1 class="display1 text-yellow headershadow text-center textmarginlog art-title">{{__('formrevisor.frase14')}}</h1>
            <h4 class="text-blue headershadow text-center my-3"> {{__('formrevisor.frase15')}} <i class="text-yellow mx-1">revisor</i>!</h4>
            
            
            @if(session()->has('message'))
            <div class="flex flex-row justify-center my2 alert alert-success">
                {{session('message')}}
            </div>
            @endif  
            
            
            <div class="col-12 col-md-6 my-4">
                <h5 class="text-blue text-start">
                    {{__('formrevisor.frase16')}} <i class="text-yellow mx-1">{{__('formrevisor.frase17')}} </i> <i class="text-yellow mx-1">{{__('formrevisor.frase18')}} </i>, {{__('formrevisor.frase19')}} <i class="text-yellow mx-1">{{__('formrevisor.frase20')}}</i> {{__('formrevisor.frase21')}}. {{__('formrevisor.frase22')}} <i class="text-yellow mx-1">{{__('formrevisor.frase23')}}</i>, <i class="text-yellow mx-1">{{__('formrevisor.frase24')}} </i> {{__('formrevisor.frase25')}} <i class="text-yellow mx-1">{{__('formrevisor.frase26')}}</i>, {{__('formrevisor.frase27')}} <i class="text-yellow mx-1">{{__('formrevisor.frase28')}}</i> {{__('formrevisor.frase29')}}
                </h5>
                <h5 class="text-blue text-start">
                    {{__('formrevisor.frase30')}} <i class="text-yellow mx-1">{{__('formrevisor.frase31')}}</i>, {{__('formrevisor.frase32')}} <i class="text-yellow mx-1">{{__('formrevisor.frase33')}}</i>. {{__('formrevisor.frase34')}} <i class="text-yellow mx-1">{{__('formrevisor.frase35')}}</i>, {{__('formrevisor.frase36')}} <i class="text-yellow mx-1">{{__('formrevisor.frase37')}}</i> {{__('formrevisor.frase38')}}.
                </h5>
            </div>
            
            <div class="col-12 col-md-6">
                <img class="img-fluid" src="/media/workwus.png" alt="">
            </div>
            
            <h4 class=" text-blue headershadow text-center  art-title">{{__('formrevisor.frase39')}}</h4>
            
            <div class="col-12 col-md-6 my-5">
                <form action="{{route('become.revisor')}}" class="justify-content-center articleshadow">
                    @csrf
                    <div class="mb-3">

                        <label for="message" class="form-label artcustomlabel">{{__('formrevisor.frase40')}}</label>
                        <textarea name="message" type="text" placeholder="{{__('formrevisor.frase41')}}" class="form-control artcustomform"></textarea>
                        <button class="btn btn-customart m-2" type="submit">{{__('formrevisor.frase42')}}</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>
    
    
</x-layout>