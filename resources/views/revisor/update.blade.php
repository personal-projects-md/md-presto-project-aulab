<x-layout>
    <x-slot name="title">Presto.it - Revisor History</x-slot>

    @if (session()->has('message'))
      <div class="flex flex-row justify-center alert alert-success">
          {{session('message')}}
      </div>  
    @endif


    <h1 class="display1 text-yellow headershadow text-center art-title">REVISOR PANEL</h1>
    <h2 class="text-blue headershadow text-center my-3">Check your History!</h2>
    <h6 class="text-cyan headershadow text-center my-3">We reccomand you to check your History in browser mode!</h6>


  <div class="container">
      <div class="row justify-content-center align-items-center text-center">
          <div class="col-12 col-md-6">

            <h3 class="text-cyan headershadow text-center mt-5">Accepted Announcements</h3>
            <div class="container over-hide articleshadow">
                <div class="row justify-content-center">
                    <div class="col-12">
                    <table class="table p-5">
                        <thead>
                            <tr>
                                <th scope="col" class="text-blue">Title</th>
                                <th scope="col" class="text-blue">Created at</th>
                                <th scope="col" class="text-blue">User</th>
                                <th scope="col" class="text-blue me-2">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($accepted_announcement as $announcement)
                              <tr>
                                  <th scope="row" class="text-blue">{{$announcement->title}}</th>
                                    <td class="text-blue">{{$announcement->created_at->format('d/m/Y')}}</td>
                                    <td class="text-blue">{{$announcement->user->name ?? ''}}</td>
                                    <td class="text-blue">
                                        <form action="{{route('revisor.null', compact('announcement'))}}" method="POST">
                                            @method('PATCH')
                                            @csrf
                                            <button class="btn" type="submit" ><i class="fas fa-undo text-cyan"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
        
            <h3 class="text-cyan headershadow text-center mt-5">Rejected Announcements</h3>
                <div class="container over-hide articleshadow mb-5">
                    <div class="row justify-content-center">
                        <div class="col-12">
                        <table class="table">
                            <thead>
                              <tr>
                                <th scope="col" class="text-blue">Title</th>
                                <th scope="col" class="text-blue">Created at</th>
                                <th scope="col" class="text-blue">User</th>
                                <th scope="col" class="text-blue">Edit</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($rejected_announcement as $announcement)
                                <tr>
                                    <th scope="row" class="text-blue">{{$announcement->title}}</th>
                                        <td class="text-blue">{{$announcement->created_at->format('d/m/Y')}}</td>
                                        <td class="text-blue">{{$announcement->user->name ?? ''}}</td>
                                        <td class="text-blue">
                                            <form action="{{route('revisor.null', compact('announcement'))}}" method="POST">
                                            @method('PATCH')
                                            @csrf
                                            <button class="btn " type="submit" ><i class="fas fa-undo text-cyan"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
          </div>
      </div>
  </div>

</x-layout>

