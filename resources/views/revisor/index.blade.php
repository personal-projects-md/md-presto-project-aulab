<x-layout>
    
    <x-slot name="title">{{__('indexRevisor.phrase1')}}</x-slot>
    
    <div class="container-fluid textmarginlog">
        <div class="row justify-content-center text-center">
            <div class="col-12">
                @if($announcement_to_check == false)
                <h1 class="display1 text-yellow headershadow text-center art-title">
                    {{__('formrevisor.frase43')}}
                </h1>

                <h2 class="text-blue headershadow text-center my-3">
                    {{__('formrevisor.frase44')}}
                </h2>

                @else
                <h1 class="display1 text-yellow headershadow text-center art-title">
                    {{__('formrevisor.frase45')}}
                </h1>
                <h2 class="text-blue headershadow text-center my-3">
                    {{__('formrevisor.frase46')}}
                </h2>
                @endif
            </div>
            
            <a href="{{route('revisor.update')}}" class="btn btn-custom2 my-5"><i class="fas fa-list-ul mx-2 text-blue"></i>History</a>
        </div>
       
        



        @if ($announcement_to_check == true)
        
        <div class="section over-hide">
            <div class="container">
                <div class="row text-center align-items-center justify-content-center">
                    <div class="col-12 col-md-4 text-center align-self-center py-5">
                        <div class="section text-center py-5 py-md-0">
                            <input class="pricing" type="checkbox" id="pricing" name="pricing" />
                            <div class="card-3d-wrap mx-auto">
                                <div class="card-3d-wrapper">
                                    <div class="card-front">
                                        <div class="pricing-wrap">
                                            
                                            <h4 class="mt-5 text-blue">{{$announcement_to_check->title}}</h4>
                                            <a href="{{route('categoryShow',['category'=>$announcement_to_check->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                                @if(session('locale')=='it')
                                                {{$announcement_to_check->category->name_it}}
                                                @elseif(session('locale')=='en')
                                                {{$announcement_to_check->category->name_en}}
                                                @elseif(session('locale')=='es')
                                                {{$announcement_to_check->category->name_es}}
                                                @endif
                                            </a>
                                            
                                            <h2 class="my-4 text-blue">{{$announcement_to_check->price}}<sup>{{__('card.cardprice')}}</sup></h2>
                                            
                                            <p class="my-2 text-blue small mt-5"><i class="fas fa-clock text-blue small"></i>Pubblicato il: 
                                                {{$announcement_to_check->created_at->format('d/m/Y')}}</p>
                                                <p class="my-2 tex-blue small"><i class="fas fa-user-circle text-blue small"></i></i>Autore: 
                                                    {{$announcement_to_check->user->name ?? ''}}</p>
                                                    
                                                    <div class="img-wrap img-2">
                                                        <img src="/media/cardbg.png" alt="">
                                                    </div>
                                                    <div class="carousel slide carousel-fade" data-bs-ride="carousel" id="showCarousel">
                                                        @if(count($announcement_to_check->images) > 0)
                                                            @foreach($announcement_to_check->images as $image)
                                                        
                                                            <div class="carousel-item @if($loop->first)active @endif">
                                                            <img src="{{$image->getUrl(250,250)}}" alt="#" class="img-wrap img-1">
                                                            </div>
                                                            @endforeach 
                                                        @else
                                                            <div class="carousel-item">
                                                            <img src="/media/noimg.png" alt="#" class="swiper-slide">
                                                          </div>
                                                        @endif
                                                    </div> 

                                                </div>
                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             
                        <div class="col-12 col-md-8">
                            <h2 class="text-yellow text-center">{{__('formrevisor.frase52')}}</h2>
                            <h4 class="mt-5 text-blue">{{$announcement_to_check->body}}</h4>
                            
                            <form action="{{route('revisor.accept_announcement' , ['announcement'=>$announcement_to_check])}}" method="POST">
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="btn btn-custom2 my-2">{{__('formrevisor.frase55')}}<i class="fa-solid fa-circle-check ms-1" style="color: green;"></i></button>
                            </form>
                            
                            <form action="{{route('revisor.reject_announcement' , ['announcement'=>$announcement_to_check])}}" method="POST">
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="btn btn-custom2 my-2">{{__('formrevisor.frase56')}} <i class="fa-solid fa-circle-xmark ms-1" style="color: red;"></i></button>
                            </form>
                        </div>
                        </div>
                        
                        
                        <div class="row justify-content-center align-items-center my-5">
                            @foreach($announcement_to_check->images as $image)
                        <div class="col-4 col-md-4 articleshadow">
                            <div class="card-body">
                                <h5 class="tc-accent">Revisione Immagini</h5>
                                <p>Nudità o atti sessuali: <span class="{{$image->adult}}"></span></p>
                                <p>Contenuto Satirico: <span class="{{$image->spoof}}"></span></p>
                                <p>Conenuto Medico: <span class="{{$image->medical}}"></span></p>
                                <p>Contenuto Violento: <span class="{{$image->violence}}"></span></p>
                                <p>Contenuto NSFW: <span class="{{$image->racy}}"></span></p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                       
                        
                       
                    </div>
                </div>
            </div>
            
            @else
            
            <div class="container mb-5">
                <div class="row align-items-center justify-content-end">
                    <div class="col-12 col-md-6 text-center">
                        <h3 class="display1 text-yellow headershadow">Good job, <i class="text-cyan">{{Auth::user()->name}}</i>!</h3>
                        <h4 class="text-blue headershadow">{{__('formrevisor.frase47')}}<i class="text-yellow mx-1">{{__('formrevisor.frase48')}}</i> {{__('formrevisor.frase49')}}!</h4>
                        <a href="{{route('announcements.index')}}" class="btn btn-custom2 m-2">{{__('formrevisor.frase50')}}</a>
                        <a href="/" class="btn btn-custom2 m-2">{{__('formrevisor.frase51')}}</a>
                    </div>
                    
                    <div class="col-12 col-md-3">
                        <img class="img-fluid" src="/media/revisorhome.png" alt="Revisor!" srcset="">
                    </div>
                </div>
            </div>
            
            
        </div>
        @endif
    </x-layout>