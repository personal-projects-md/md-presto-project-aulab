<x-layout>

  <x-slot name="title">Presto.it - Login</x-slot>
  
  <h2 class="display1 text-yellow headershadow text-center textmarginlog log-title">{{__('login.frase1')}}</h2>
  <h4 class="text-blue headershadow text-center"><i class="text-yellow mx-1">Boost</i> {{__('login.frase3')}} </h4>

  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
      <li>{{$error}}</li>
    @endforeach
    </ul>
  </div>
  @endif

  
  <div class="container margincustomlog">
    <div class="row justify-content-center align-items-center">
      <div class="col-12 col-md-2"></div>
      <div class="col-12 col-md-4 customheight">
        <form method="POST" action="{{route('login')}}">
          @csrf
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label logincustomlabel">{{__('login.frase4')}}</label>
            <input name="email" type="email" class="form-control logincustomform" id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label logincustomlabel">Password</label>
            <input name="password" type="password" class="form-control logincustomform" id="exampleInputPassword1">
          </div>
          <button type="submit" class="btn btn-customlog mt-3"> {{__('login.frase5')}} </button>
        </form>
        <a href="{{route('register')}}"><h6 class="text-grey small loghover mt-2 text-center">{{__('login.frase6')}}</h6></a> 
      </div>
      
      <div class="col-12 col-md-6">
        <img src="/media/login.png" class="img-fluid bglogreg" alt="">
      </div>
      
    </div>
  </div> 
  
</x-layout>