<x-layout>

  <x-slot name="title">Presto.it - Register</x-slot>

  <h2 class="display1 text-yellow headershadow text-center textmarginlog log-title">{{__('login.frase7')}}</h2>
  <h4 class="text-blue headershadow text-center"> {{__('login.frase8')}} <i class="text-yellow mx-1">{{__('login.frase9')}}</i>{{__('login.frase10')}}</h4>
  
  <div class="container margincustomlog">
    <div class="row justify-content-center align-items-center">
      <div class="col-12 col-md-2"></div>
      <div class="col-12 col-md-4">
        <img src="/media/register.png" class="img-fluid bglogreg" alt="">
      </div>
      <div class="col-12 col-md-4 customheight">
        <form method="POST" action="{{route('register')}}">
          @csrf
          <div class="mb-3">
            <label for="name" class="form-label logincustomlabel">{{__('login.frase11')}}</label>
            <input name="name" type="text" class="form-control logincustomform" id="name" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label logincustomlabel">{{__('login.frase4')}}</label>
            <input name="email" type="email" class="form-control logincustomform" id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label logincustomlabel">Password</label>
            <input name="password" type="password" class="form-control logincustomform" id="exampleInputPassword1">
          </div>
          <div class="mb-3">
            <label for="password_confirmation" class="form-label logincustomlabel">{{__('login.frase12')}} password</label>
            <input name="password_confirmation" type="password" class="form-control logincustomform" id="password_confirmation">
          </div>
          <button type="submit" class="btn btn-customlog mt-3">{{__('login.frase7')}}</button>
        </form>
        <a href="{{route('login')}}"><h6 class="text-grey small loghover mt-2 text-center"> {{__('login.frase13')}} </h6></a> 
      </div>
      <div class="col-12 col-md-2"></div>
    </div>
  </div> 
  
</x-layout>



