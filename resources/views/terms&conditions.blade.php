<x-layout>
    <x-slot name="title">Presto.it - Terms & Conditions</x-slot>
    
    <h2 class="display1 text-center text-yellow headershadow margincustomhome">{{__('ftlinks.termstext')}}</h2>

   <div class="container articleshadow my-5">
       <div class="row text-center text-blue">
           <div class="col-12">
                <p>
                     {{__('ftlinks.terms')}}
                </p>
           </div>
       </div>
   </div>

   <h2 class="display1 text-center text-yellow headershadow margincustomhome">{{__('ftlinks.termstext2')}}</h2>

   <div class="container articleshadow my-5">
       <div class="row text-center text-blue">
           <div class="col-12">
                <p>
                     {{__('ftlinks.privacy')}}
                </p>
           </div>
       </div>
   </div>

   <h2 class="display1 text-center text-yellow headershadow margincustomhome">{{__('ftlinks.termstext3')}}</h2>

   <div class="container articleshadow my-5">
       <div class="row text-center text-blue">
           <div class="col-12">
                <p>
                     {{__('ftlinks.cookie')}}
                </p>
           </div>
       </div>
   </div>
    
</x-layout>