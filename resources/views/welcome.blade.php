<x-layout>

    <x-header/>

    <section class="px-0 container-fluid">
        <div class="container-fluid margincustomhome">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-4 text-center">
                    <i class="fas mx-3 fa-couch fa-2x text-yellow"></i>
                    <i class="fas mx-3 fa-futbol fa-2x text-blue"></i>
                    <i class="fas mx-3 fa-paw fa-2x text-yellow"></i>
                    <i class="fas mx-3 fa-chess-knight fa-2x text-blue"></i>
                    <i class="fas mx-3 fa-book fa-2x text-yellow"></i>
                </div>
                <div class="col-12 col-md-4 my-4">
                    <h3 class="display1 text-yellow headershadow text-center">{{__('welcome.language6')}}</h3>
                    <h4 class="text-blue headershadow text-center">{{__('welcome.language7')}}</h4>
                </div>
                <div class="col-12 col-md-4 text-center">
                    <i class="fas mx-3 fa-2x text-yellow fa-laptop"></i>
                    <i class="fas mx-3 fa-2x text-blue fa-paint-brush"></i>
                    <i class="fas mx-3 fa-2x text-yellow fa-tshirt"></i>
                    <i class="fas mx-3 fa-2x text-blue fa-plane"></i>
                    <i class="fas mx-3 fa-2x text-yellow fa-car"></i>
                </div>

                

                
                @foreach ($announcements as $announcement)
                @if($announcement->is_accepted == true) 
  
                <div class="col-12 col-md-3 cardmargin my-5">
                    <div class="section text-center py-5 py-md-0">
                        <div class="card-3d-wrap mx-auto">
                            <div class="card-3d-wrapper">
                                <div class="card-front">
                                    <div class="pricing-wrap">
                                        
                                        <h4 class="mt-5 text-blue">{{$announcement->title}}</h4>
                                        {{-- <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>{{$announcement->category->name}}</a> --}}
                                        @if(session('locale')=='it')
                                        <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                            {{($announcement->category->name_it)}}
                                        </a>
                                        @elseif(session('locale')=='en')
                                        <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                            {{($announcement->category->name_en)}}
                                        </a>
                                        @elseif(session('locale')=='es')
                                        <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="my-2 text-cyan"><i class="fas fa-hashtag text-cyan"></i>
                                            {{($announcement->category->name_es)}}
                                        </a>
                                        @endif

                                        <h2 class="my-4 text-blue">{{$announcement->price}}<sup>{{__('card.cardprice')}}</sup></h2>
                                        
                                        <p class="my-2 text-blue small mt-5"><i class="fas fa-clock text-blue small"></i>
                                        {{__('card.cardUpdated')}} {{$announcement->created_at->format('d/m/Y')}}</p>
                                        <p class="my-2 text-blue small"><i class="fas fa-user-circle text-blue small"></i></i>{{__('card.cardAuthor')}} {{$announcement->user->name ?? ''}}</p>

                                        <a href="{{route('announcements.detail', compact('announcement'))}}" class="btn btn-custom2">{{__('card.cardButton')}}</a>
                                        
                                        <div class="img-wrap img-2">
                                            <img src="/media/cardbg.png" alt="">
                                        </div>
                                        <!--Swiper Immagini Annuncio-->

                                        <div class="carousel slide carousel-fade" data-bs-ride="carousel" id="showCarousel">
                                            @if(count($announcement->images) > 0)
                                                @foreach($announcement->images as $image)
                                            
                                                <div class="carousel-item @if($loop->first)active @endif">
                                                <img src="{{$image->getUrl(250,250)}}" alt="#" class="img-wrap img-1">
                                                </div>
                                                @endforeach 
                                            @else
                                                <div class="carousel-item">
                                                <img src="/media/noimg.png" alt="#" class="swiper-slide">
                                              </div>
                                            @endif
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </section>

    <x-teamhome />

    <x-workhome />

</x-layout>    