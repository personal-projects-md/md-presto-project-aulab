<?php

    return [
        'CatShoT0' => 'Presto.it - Categoria',
        'CatShoT1' => 'ESPLORA PER CATEGORIA!',
        'CatShoT2' => 'Benvenuto nella categoria',
        'CatShoT3' => 'Possiamo aiutarti?',
        'CatShoT4' => 'Inserisci un annuncio',
        'CatShoT5' => 'Filtra per Categoria',
        'CatShoT6' => 'Tool di',
        'CatShoT7' => 'ricerca',
        'CatShoT8' => ',al tuo servizio!',
        'CatShoT9' => 'Scopri di più',
        'CatShoT10' => 'Non sono presenti annunci interessanti per questa categoria...',
        'CatShoT11' => 'Ma potresti essere il primo ad inserirne uno!',
        'CatShoT12' => 'Nulle di interessante?',
        'CatShoT13' => 'Torna agli Annunci',
    ];