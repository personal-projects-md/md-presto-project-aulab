<?php

return[
    'phrase1'=>'Presto.it - Area revisore',
    'phrase2'=>"Cos'hanno inserito gli utenti in tua assenza?",
    'phrase3'=>'Complimenti! Hai revisionato tutti gli annunci!',
    'phrase4'=>'Revisiona gli annunci',
    'phrase5'=>'Sei il revisor più veloce di Presto.it!',
    'phrase6'=>' Pubblicato il: ',
    'phrase7'=>' Autore: ',
    'phrase8'=>'Descrizione articolo: ',
    'phrase9'=>'Accetta',
    'phrase10'=>'Rifiuta',
    'phrase11'=>'Ottimo lavoro,',
    'phrase12'=>'Sono in arrivo nuovi annunci da revisionare',
    'phrase13'=>'Sezione Annunci',
    'phrase14'=>'Torna alla Home',

];