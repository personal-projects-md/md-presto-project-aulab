<?php

return[

    'allAnnouncements' => 'UN MONDO DI ARTICOLI TI ASPETTA!',
    'allAnnouncements1'=>'Metti il ',
    'allAnnouncements2'=>'turbo ',
    'allAnnouncements3'=>'e cerca il prodotto che fa per te!',
    'allAnnouncements4'=>'o Effettua il Login',
    'allAnnouncements5'=>'Bentornato',
    'allAnnouncements6'=>'Allaccia le cinture e ',
    'allAnnouncements7'=>'accelera',
    'allAnnouncements8'=>"verso l'offerta migliore!",
    'allAnnouncements9'=>'Sfoglia gli annunci',
    'allAnnouncements10'=>'Inserisci un annuncio',
    'ht' => 'IL NOSTRO TEAM',
    'ht1' => 'Le menti dietro Presto.it!',
    'ht2' => 'PERSONE, COME TE!',
    'ht3' => 'Collezionisti e... ',
    'ht4' => 'Sviluppatori!',
    'ht5' => '',
    'ht6' => 'Team',

    'wt' => 'LAVORA CON NOI',
    'wt1' => 'Entra nel team di Presto.it!',
    'wt2' => 'INOLTRA LA TUA RICHIESTA!',
    'wt3' => 'Modera la ',
    'wt4' => 'community',
    'wt5' => ', comodamente da casa tua!',
    'wt6' => 'Compila il form!',





];