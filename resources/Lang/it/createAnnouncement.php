<?php

    return [
        'CreAnn0' => 'Presto.it - Inizia a vendere!',
        'CreAnn1' => 'INSERISCI UN ARTICOLO',
        'CreAnn2' => 'Cosa',
        'CreAnn3' => 'vendiamo',
        'CreAnn4' => 'oggi?',
        'CreAnn5' => 'Titolo Annuncio',
        'CreAnn6' => 'Descrizione Annuncio',
        'CreAnn7' => 'Prezzo',
        'CreAnn8' => 'Categoria',
        'CreAnn9' => 'Sfoglia...',
        'CreAnn10' => 'Carica almeno tre immagini!',
        'CreAnn11' => 'Ti consigliamo il formato',
        'CreAnn12' => '.png',
        'CreAnn13' => 'Preview',
        'CreAnn14' => 'delle tue immagini',
        'CreAnn15' => 'Ricorda che verranno',
        'CreAnn16' => 'ridimensionate',
        'CreAnn17' => 'in automatico!',
        'CreAnn18' => 'Crea Annuncio',
        'annbtn' => 'Rimuovi'
    ];