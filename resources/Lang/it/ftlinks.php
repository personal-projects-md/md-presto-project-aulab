<?php
return [
    'termstext' => 'Termini & Condizioni',
    'terms' => "ll presente documento riporta i termini e le condizioni generali di utilizzo del sito web Presto.it che offre e-commerce con possibilità di
    compravendita online forniti da un fornitore terzo.
    
    - Definizioni
    Per consentire una completa comprensionee accettazione dei presenti termini e condizioni, i seguenti termini, al singolare e al
    plurale, avranno il significato di seguito indicato.
    - Titolare: Presto.it -BACKEND-STREET BOYS con sede legale in Aulab, Partita IVA/ Codice Fiscale HACKADEMY46, H46, capitale
    sociale interamente versato 10,00 €, indirizzo PEC backendstreetboys@aulab.t
    Applicazione: il sito web Presto.it
    
    I Titolari non assumono alcuna responsabilità per eventuale uso fraudolento e illecito che possa essere tatto da parte di terzi delle
    carte di credito e altri mezzi di pagamento,
    I Titolare non sarà responsabile per:
    eventuali perdite di opportunità commerciale e qualsiasi altra perdita, anche indiretta, eventualmente subita dall'Utente che
    non siano conseguenza diretta della violazione del contratto da parte del Titolare
    errato o inidoneo utilizzo del'Applicazione da parte degli Utenti o di terzi
    In nessun caso il Titolare potra essere ritenuto responsabile per una somma superiore al doppio del costo pagato dalrutente.
    
    - Forza maggiore
    Titolare non potrà essere considerato responsabile per il mancato o ritardato adempimento delle proprie obbligazioni, per
    Circostanze al di Tuori del controllo ragionevole del tolare dovute ad eventi di forza maggiore o, comunque, ad eventi imprevIsti
    ed imprevedibili e, comunque, indipendenti dalla sua volontà.
    L'adempimento delle obbligazioni da parte del Titolare si intenderà sospeso per il periodo in cui si verificano eventi di torza
    maggiore.
    I Titolari compiranno qualsiasi atto in loro potere al fine di individuare soluzioni che consentano il corretto adempimento delle
    proprie obbligazioni nonostante la persistenza di eventi di forza maggiore.
    
    - Collegamento a siti di terzi
    LApplicazione potrebbe contenere collegamenti a siti/applicazioni di terzi. il Titolare non esercita alcun controllo su di essi e,
    pertanto, non e in aicun modo responsabie per i Contenut di questu Si/applicazionl.
    Alcuni di questi collegamenti potrebbero rinviare a siti/applicazioni di terzi che forniscono servizi attraverso l'Applicazione. In
    questi casi, ai singoli servizi si applicheranno le condizioni generali per fuso del sito/applicazione e per la fruizione del servizio
    predisposte dai terzi, rispetto alle quali il Titolare non assume alcuna responsabilità.
    
    - Legge applicabile e foro competente
    Le Condizioni sono soggette alla legge italiana.
    Per gli utenti Professionisti, per ogni controversia relativa alla Applicazione, esecuzione e interpretazione delle presenti Condizioni
    è competente il foro del luogo in cui ha sede il Titolare.
    Per gli Utenti Consumatori ogni controversia relativa all'applicazione, esecuzione e interpretazione delle presenti Condizioni sarà
    devoluta al foro del luogo in cui rUtente Consumatore risiede o ha eletto domicilio, se ubicati nel territorio dello Stato Italiano,
    salva la facoltà per rUtente Consumatore di adire un giudice diverso da quello del 'foro del consumatore' ex art. 66 bis del Codice
    del Consumo, competente per territorio secondo uno dei criteri di cui agli art. 18, 19e 20 del codice processuale civile.
    E fatta salva l'applicazione agi Utenti Consumatori che non abbiano la loro residenza abituale in Italia delle disposizioni
    eventualmente più favorevoli e inderogabili previste dalla legge del paese in cui essi hanno la loro residenza abituale, in
    particolare in relazione al termine per l'esercizio del diritto di recesso, al termine per la restituzione dei Prodotti, in caso di
    esercizio di tale diritto, alle modalità e formalità della comunicazione del medesimo e alla garanzia legale di conformità.
    14. Risoluzione delle controversie online per Utenti Consumatori
    tUtente Consumatore residente in Europa deve essere a conoscenza del fatto che la Commissione Europea ha istituito una
    piattaforma online che fornisce uno strumento di risoluzione alternativa delle controversie. Tale strumento può essere utilizzato
    dall'utente Consumatore per risolvere in via non giudiziale ogni controversia relativa a esso derivante da contratti di vendita di beni
    e fornitura di servizi stipulati in rete. Di conseguenza, rUtente Consumatore può usare tale piattaforma per la risoluzione di ogni
    disputa nascente dal contratto stipulato online. La piattaforma é disponibile al seguente indirizzo: ec.europa.eu/consumers/odr/
    Data 29/05/2022",


    'termstext2' => 'Privacy Policy',
    'privacy' => "Lo scopo del presente documento è di informare la persona fisica (di seguito 'Interessato') relativamente al trattamento dei suoi dati
    personali (di seguito 'Dati Personali') raccolti dal titolare del trattamento, ABC S.p.a, con sede legale in Via Torino 1, 20100 Milano,
    CF/Partita VA 0123456789C, indirizzo e-mail impresa@email.it, indirizzo PEC impresa@pec.it, telefono 800 123 123, (di seguito
    Titolare), tramite il Sito web www.esempio.it (di seguito 'Applicazione').
    Le modifiche e gli aggiornamenti saranno vincolanti non appena pubblicati sull'Applicazione. In caso di mancata accettazione delle
    modifiche apportate all'informativa Privacy, I'interessato è tenuto a cessare l'utilizzo di questa Applicazione e puo richiedere al Titolare di
    cancellare i propri Dati Personali.
    Categorie di Dati Personali trattati
    Il Titolare tratta le seguenti tipologie di Dati Personali forniti volontariamente dall'interessato
    Dati di contatto: nome, cognome, indirizzo, e-mail, telefono, immagini, credenziali di autenticazione, eventuali ulteriori
    informazioni inviate dal interessato, etc.
    Dati fiscalie di pagamento: codice fiscale, patita IVA, dati della carta di credito, estremi del conto corrente bancario, etc.",

    'termstext3' => 'Cookie Policy',
    'cookie' => "Questo sito web utilizza i cookie
    Noi e terze parti usiamo cookie o tecnologie simili per funzionalità tecniche e, con il tuo consenso, anche per altre finalità descritte nella Cookie Policy quali raccogliere ed elaborare dati personali dai dispositivi, mostrarti pubblicità personalizzata, misurarne la performance, analizzare le nostre audience e migliorare i nostri prodotti e servizi. Puoi liberamente prestare, rifiutare o revocare il tuo consenso in qualsiasi momento, personalizzando le tue preferenze. Cliccando sul pulsante 'Accetta tutti i cookie' acconsenti all'uso di tali tecnologie per tutte le finalità indicate. Cliccando sul pulsante 'Accetta cookie tecnici' acconsenti all'uso dei soli cookie tecnici.",

    'contacts' => 'CONTATTACI',
  
];