<?php 

return [
    'ftText1' => 'E\' un progetto che nasce dalla voglia di realizzare un ',
    'ftTextH1' => ' e-commerce ',
    'ftT1s' => 'rivoluzionario per dare un ',
    'ftTextH2' => ' boost ',
    'ftText1t' => 'agli utenti per la ricerca dei loro prodotti preferiti.',
    'ftText2' => 'Tutto in completa ',
    'ftTextH3' => ' sicurezza',
    'ftTerms' => 'Termini & Condizioni',
    'ftHelp' => 'Assistenza',
    'ftWho' => 'Chi Siamo',
    'ftContact' => 'Contatti',
    'ftWork' => 'Lavora con noi',
];