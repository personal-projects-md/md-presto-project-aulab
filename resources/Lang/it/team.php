<?php

return [

    'marco' => 'Il Backendista di fiducia di Presto! Un programmatore che ha sviluppato enormi conoscenze e capacità in breve tempo. Una risorsa preziosa, anche in Frontend. Appassionato di informatica, programmazione e videogames.',
    'antonio' => 'Definito il "Frontendista di sfondamento" dal resto del team, è uno sviluppatore testardo, curioso ed entusiasta. Se la cavicchia anche col Backend, ma, del resto, al cuor non si comanda. Appassionato di fumetto, manga e videogames.',
    'peter' => 'Sviluppatore meticoloso ed attento, quel che conta è portare il risultato a casa, ma soprattutto farlo bene! Di giorno sviluppatore, di notte
    appassionato di programmazione, belle auto e Gundam.',
    'andrea' => 'L\'uomo delle soluzioni. Nel suo silenzio si nascondono ragionamenti precisi e mirati a risolvere i problemi. Presto.it gli deve tanto! E\' anche l\'uomo più in forma del team, anche grazie alla sua passione per il fitness!',
    'matteo' => 'Impossibile buttarlo giù. Ritmi stressanti ed orari di lavoro non sono un problema per lui. E\' uno sviluppatore sempre pronto a dare il massimo per aiutare il team. Appassionato di anime, manga e videogames.',

];