<?php

    return [
        'CreAnn0' => 'Colocar anuncio',
        'CreAnn1' => 'INSERTAR UN ARTÍCULO',
        'CreAnn2' => '¿Qué',
        'CreAnn3' => 'vemos',
        'CreAnn4' => 'hoy?',
        'CreAnn5' => 'Título del anuncio',
        'CreAnn6' => 'Descripción del anuncio',
        'CreAnn7' => 'Precio',
        'CreAnn8' => 'Categoría',
        'CreAnn9' => 'Navegar...',
        'CreAnn10' => '¡Sube al menos tres imágenes!',
        'CreAnn11' => 'Recomendamos el formato',
        'CreAnn12' => '.png',
        'CreAnn13' => 'Preview',
        'CreAnn14' => 'de tus imágenes',
        'CreAnn15' => '¡Recuerda que se',
        'CreAnn16' => 'redimensionarán',
        'CraAnn17' => 'automáticamente!',
        'CreAnn18' => 'Crear anuncio',
        'annbtn' => 'Retirar'
    ];