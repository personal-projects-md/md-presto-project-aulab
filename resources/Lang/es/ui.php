<?php

return[
    'allAnnouncements'=>'UN MUNDO DE ARTÍCULOS TE ESPERA!',
    'allAnnouncements1'=>'Ponga el',
    'allAnnouncements2'=>'turbo',
    'allAnnouncements3'=>'y busque el producto adecuado para usted!',
    'allAnnouncements4'=>'o Conéctese',
    'allAnnouncements5'=>'Bienvenido de nuevo',
    'allAnnouncements6'=>'Abróchense los cinturones de seguridad y',
    'allAnnouncements7'=>'acelera ',
    'allAnnouncements8'=>'hacia la mejor oferta!',
    'allAnnouncements9'=>'Busque listados ',
    'allAnnouncements10'=>'Enviar un anuncio',
    'ht' => 'NUESTRO EQUIPO',
    'ht1' => '¡Las mentes detrás de Presto.it!',
    'ht2' => '¡GENTE COMO TÚ!',
    'ht3' => 'Personas antes de ser ',
    'ht4' => 'desarrolladores!',
    'ht5' => '',
    'ht6' => 'Team',

    'wt' => 'TRABAJA CON NOSOTROS',
    'wt1' => 'Únete al equipo de Presto.it!',
    'wt2' => 'ENVIAR LA SOLICITUD',
    'wt3' => 'Modera la ',
    'wt4' => 'communidad',
    'wt5' => ', en Smart Working!',
    'wt6' => 'CLIC AQUÍ',



];
