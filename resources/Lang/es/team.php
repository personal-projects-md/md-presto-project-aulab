<?php

return [

    'marco' => '¡El desarrollador backend de confianza de Presto! Un programador que ha desarrollado enormes conocimientos y habilidades en poco tiempo. Un recurso precioso incluso en Frontend. Hobbies: Informática, programación y videojuegos.',

    'antonio' => 'Conocido como el "Desarrollador Frontend Innovador" por el resto del equipo, es un desarrollador testarudo, curioso y entusiasta. También juega con el Backend, pero, después de todo, no puedes gobernar tu corazón. Aficiones: cómics, manga y videojuegos.',

    'peter' => 'Desarrollador meticuloso y atento, lo que importa es llevar el resultado a casa, pero sobre todo ¡hacerlo bien! Revelador durante el día, por la noche
    apasionado por la programación, los autos hermosos y los Gundams.',

    'andrea' => 'El hombre de las soluciones. En su silencio se esconden razonamientos precisos y encaminados a la solución de problemas. ¡Presto.it le debe tanto! ¡También es el hombre más en forma del equipo, gracias a su pasión por el fitness!',

    'matteo' => 'Imposible derribarlo. Los ritmos estresantes y los horarios de trabajo no son un problema para él. Es un desarrollador que siempre está dispuesto a dar lo mejor de sí para ayudar al equipo. Apasionado del anime, el manga y los videojuegos.',

];