<?php

return[
    'phrase1'=>'Auditor',
    'phrase2'=>"¿Qué han publicado los usuarios en tu ausencia?",
    'phrase3'=>'¡Felicidades! Ha revisado todos los anuncios',
    'phrase4'=>'Revisas los anuncios',
    'phrase5'=>'¡Eres el revisor de Presto.it más rápido!',
    'phrase6'=>'Publicado en:',
    'phrase7'=>'Autor:',
    'phrase8'=>'Descripción del articulo:',
    'phrase9'=>'Aceptar',
    'phrase10'=>'Rechazar',
    'phrase11'=>'Excelente trabajo,',
    'phrase12'=>'Nuevos anuncios están en camino para que los revise',
    'phrase13'=>'Sección anuncios',
    'phrase14'=>'Volver a la homepage',

];