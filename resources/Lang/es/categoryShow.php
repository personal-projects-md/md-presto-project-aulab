<?php

    return [
        'CatShoT0' => 'Explorar por categoría',
        'CatShoT1' => '¡EXPLORA POR CATEGORÍA!',
        'CatShoT2' => 'Bienvenido a la categoría',
        'CatShoT3' => '¿Podemos ayudarte?',
        'CatShoT4' => 'Colocar un anuncio',
        'CatShoT5' => 'Filtrar por Categoría',
        'CatShoT6' => '¡Tool de',
        'CatShoT7' => 'búsqueda',
        'CatShoT8' => ',a su servicio!',
        'CatShoT9' => 'Saber más',
        'CatShoT10' => 'No hay anuncios para esta categoría...',
        'CatShoT11' => '¡Pero podrías ser el primero en publicar uno!',
        'CatShoT12' => 'Parece que esta categoría está vacía...',
        'CatShoT13' => 'Volver a anuncios'
    ];