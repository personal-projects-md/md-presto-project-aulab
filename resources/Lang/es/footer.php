<?php 

return [
    'ftText1' => 'Es un proyecto que nace de la voluntad de crear un ',
    'ftTextH1' => ' e-commerce ',
    'ftT1s' => 'revolucionario para dar un ',
    'ftTextH2' => ' boost ',
    'ftText1t' => 'a los usuarios para buscar sus productos favoritos.',
    'ftText2' => 'Todo con total ',
    'ftTextH3' => ' securidad',
    'ftTerms' => 'Términos y Condiciones',
    'ftHelp' => 'Servicio al Cliente',
    'ftWho' => 'Team',
    'ftContact' => 'Contactos',
    'ftWork' => 'Trabaja con nosotros',
];