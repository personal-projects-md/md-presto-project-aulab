<?php
return [
    'termstext' => 'Términos y Condiciones',
    'terms' => "Este documento contiene los términos y condiciones generales de uso del sitio web Presto.it que ofrece comercio electrónico con la posibilidad de
    comercio en línea proporcionado por un proveedor externo.
    
    - Definiciones
    Para permitir una completa comprensión y aceptación de estos términos y condiciones, los siguientes términos, en singular y al
    plural, tendrá el significado que se indica a continuación.
    - Propietario: Presto.it -BACKEND-STREET BOYS con domicilio social en Aulab, número de IVA / Código fiscal HACKADEMY46, H46, capital
    totalmente pagado € 10,00, dirección PEC backendstreetboys@aulab.t
    Aplicación: el sitio web de Presto.it
    
    Los Titulares no asumen responsabilidad alguna por el uso fraudulento o ilícito que puedan hacer terceros de los
    tarjetas de crédito y otros medios de pago,
    El Titular no será responsable de:
    cualquier pérdida de oportunidad comercial y cualquier otra pérdida, incluso indirecta, que pueda sufrir el Usuario que
    no son consecuencia directa de la violación del contrato por parte del Titular
    uso incorrecto o inadecuado de la Aplicación por parte de los Usuarios o de terceros
    En ningún caso el Titular podrá ser considerado responsable de una suma superior al doble del coste pagado por el usuario.
    
    - Fuerza mayor
    El Titular no podrá ser considerado responsable por el incumplimiento o retraso en el cumplimiento de sus obligaciones, por
    Circunstancias ajenas al control razonable del tolare debidas a supuestos de fuerza mayor o, en todo caso, a caso fortuito
    e imprevisible y, en todo caso, independiente de su voluntad.
    El cumplimiento de las obligaciones por parte del Titular se considerará suspendido por el período en que ocurran los hechos de torza.
    mayor que.
    Los Titulares realizarán cualquier acto a su alcance con el fin de identificar soluciones que permitan el correcto cumplimiento de los
    sus obligaciones a pesar de la persistencia de eventos de fuerza mayor.
    
    - Enlace a sitios de terceros
    La Aplicación puede contener enlaces a sitios/aplicaciones de terceros. el Titular no ejerce ningún control sobre los mismos y,
    por lo tanto, no es de ninguna manera responsable por los contenidos de esta Sí/aplicación.
    Algunos de estos enlaces pueden hacer referencia a sitios/aplicaciones de terceros que brindan servicios a través de la Aplicación. En
    en estos casos, las condiciones generales para el sitio / aplicación y para el uso del servicio se aplicarán a los servicios individuales
    elaborados por terceros, respecto de los cuales el Titular no asume ninguna responsabilidad.
    
    - Ley aplicable y tribunal competente
    Las Condiciones están sujetas a la ley italiana.
    Para usuarios Profesionales, para cualquier disputa relacionada con la Aplicación, ejecución e interpretación de estas Condiciones
    el tribunal del lugar donde se encuentra el Controlador de datos es competente.
    Para los Usuarios Consumidores, cualquier controversia relativa a la aplicación, ejecución e interpretación de las presentes Condiciones será
    delegado al foro del lugar donde el Usuario Consumidor reside o ha elegido domicilio, si se encuentra en el territorio del Estado italiano,
    sin perjuicio del derecho del Usuario Consumidor a recurrir a un juez distinto al del 'foro de consumidores' de conformidad con el art. 66 bis del Código
    del Consumo, competente para el territorio según uno de los criterios a que se refiere el art. 18, 19 y 20 del código de procedimiento civil.
    Y sin perjuicio de la aplicación de las disposiciones a los Usuarios Consumidores que no tengan su residencia habitual en Italia
    posiblemente más favorables y obligatorias previstas por la ley del país en el que tengan su residencia habitual, en
    particularmente en relación con el plazo para ejercer el derecho de desistimiento, el plazo para devolver los Productos, en caso de
    ejercicio de este derecho, los modos y formalidades de su comunicación y la garantía legal de conformidad.
    14. Resolución de disputas en línea para usuarios consumidores
    El Usuario Consumidor residente en Europa debe ser consciente de que la Comisión Europea ha puesto en marcha un
    plataforma en línea que proporciona una herramienta alternativa de resolución de disputas. Tal herramienta se puede utilizar
    por el Usuario Consumidor para resolver de forma extrajudicial cualquier controversia relacionada con él derivada de los contratos de compraventa de bienes
    y suministro de los servicios estipulados en la red. En consecuencia, el Usuario Consumidor puede utilizar esta plataforma para la resolución de cualquier
    controversia derivada del contrato estipulado en línea. La plataforma está disponible en la siguiente dirección: ec.europa.eu/consumers/odr/
    Fecha 29/05/2022",

    'termstext2' => 'Privacy Policy',
    'privacy' => "El presente documento tiene como finalidad informar a la persona física (en adelante el 'Interesado') respecto al tratamiento de sus datos
    datos personales (en adelante, 'Datos personales') recopilados por el controlador de datos, ABC S.p.a, con domicilio social en Via Torino 1, 20100 Milán,
    CF / Partita VA 0123456789C, dirección de correo electrónico company@email.it, dirección PEC company@pec.it, teléfono 800 123 123, (en adelante
    Propietario), a través del sitio web www.example.it (en adelante, la 'Aplicación').
    Los cambios y actualizaciones serán vinculantes tan pronto como se publiquen en la Aplicación. En caso de no aceptación de la
    cambios realizados a la Política de Privacidad, el interesado está obligado a dejar de usar esta Aplicación y puede solicitar al Titular de
    eliminar sus Datos Personales.
    Categorías de datos personales procesados
    El Responsable del Tratamiento trata los siguientes tipos de Datos Personales facilitados voluntariamente por el interesado
    Datos de contacto: nombre, apellidos, dirección, e-mail, teléfono, imágenes, credenciales de autenticación, cualquier adicional
    información enviada por el interesado, etc.
    Datos fiscales y de pago: código fiscal, certificado de IVA, datos de tarjetas de crédito, datos de cuentas bancarias, etc.",

    'termstext3' => 'Cookie Policy',
    'cookie' => "Este sitio web utiliza cookies
    Nosotros y terceros utilizamos cookies o tecnologías similares para la funcionalidad técnica y, con su consentimiento, también para otros fines descritos en la Política de cookies, como recopilar y procesar datos personales de dispositivos, mostrarle publicidad personalizada, medir su rendimiento, analizar nuestras audiencias y mejorando nuestros productos y servicios. Puede prestar, rechazar o retirar libremente su consentimiento en cualquier momento personalizando sus preferencias. Al hacer clic en el botón 'Aceptar todas las cookies', acepta el uso de estas tecnologías para todos los fines indicados. Al hacer clic en el botón 'Aceptar cookies técnicas', acepta el uso exclusivo de cookies técnicas.",

    'contacts' => 'PONTE EN CONTACTO CON NOSOSTROS',
];