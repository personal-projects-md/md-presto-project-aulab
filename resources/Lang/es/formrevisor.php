<?php

return[ 
    
    
    'frase14' => "¡TRABAJA CON NOSOTROS!",
    'frase15' => "Forma parte de nuestro equipo, y conviértete",
    'frase16' => "Creemos en el valor de las",
    'frase17' => "habilidades y la",
    'frase18' => "meritocracia",
    'frase19' => "con un espíritu",
    'frase20' => "inclusivo",
    'frase21' => "y transparente",
    'frase22' => "Hemos construido un entorno de trabajo",
    'frase23' => "estimulante",
    'frase24' => "flexible",
    'frase25' => "y",
    'frase26' => "heterogéneo",
    'frase27' => "atenta a las personas y sus necesidades y enfocada en el desarrollo de",
    'frase28' => "profesionalismo",
    'frase29' => "y el intercambio de resultados.",
    'frase30' => "Valoramos mucho la",
    'frase31' => "diversidad de puntos de vista",
    'frase32' => "la confrontación y el",
    'frase33' => "trabajo en equipo",
    'frase34' => "Buscamos personas atentas a los",
    'frase35' => "detalles",
    'frase36' => "que piensen con rigor, que no teman",
    'frase37' => "innovar",
    'frase38' => "y que amen comunicarse de manera simple y efectiva",
    'frase39' => "RELLENAR EL FORMULARIO",
    'frase40' => "¡Cuéntanos acerca de tí!",
    'frase41' => "¡Cuéntanos brevemente por qué te gustaría trabajar con nosotros!",
    'frase42' => "¡Envíe su solicitud!",
    'frase43' => "¡Bien hecho! ¡Has revisado todos los anuncios!",
    'frase44' => "¡Eres el crítico más rápido de Presto.it!",
    'frase45' => "¿Qué ingresaron los usuarios en tu ausencia?",
    'frase46' => "Revisa los anuncios",
    'frase47' => "Los nuevos están en camino",
    'frase48' => "anuncios",
    'frase49' => "para ser revisado!",
    'frase50' => "Sección de Anuncios",
    'frase51' => "De vuelta a home",
    'frase52' => "Descripción del articulo",
    'frase55' => "Aceptar",
    'frase56' => "Rechazar",






];