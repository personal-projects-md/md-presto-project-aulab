<?php

return [

    'marco' => 'Presto\'s trusted Backend Developer! A programmer who has developed enormous knowledge and skills in a short time. A precious resource even in Frontend. Hobbies: Computer science, programming and videogames.',

    'antonio' => 'Referred to as the "Breakthrough Frontend Developer" by the rest of the team, he is a stubborn, curious and enthusiastic developer. He also tinkers with the Backend, but, after all, you can\'t rule your heart. Hobbies: comics, manga and videogames.',

    'peter' => 'Meticulous and attentive developer, what matters is to bring the result home, but above all to do it well! Developer during the day, at night
    passionate about programming, beautiful cars and Gundams.',

    'andrea' => 'The man of solutions. In his silence, precise reasoning is hidden and aimed at solving problems. Presto.it owes him so much! He is also the fittest man on the team, thanks to his passion for fitness!',

    'matteo' => 'Impossible to knock it down. Stressful rhythms and working hours are not a problem for him. He is a developer who is always ready to give his best to help the team. Passionate about anime, manga and videogames.',

];