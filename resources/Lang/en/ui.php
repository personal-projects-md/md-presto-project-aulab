<?php

return[
    'allAnnouncements'=>'A WORLD OF ARTICLES AWAITS YOU!',
    'allAnnouncements1'=>'Get the ',
    'allAnnouncements2'=>'boost ',
    'allAnnouncements3'=>"and search for the best offer!",
    'allAnnouncements4'=>'or Log-in',
    'allAnnouncements5'=>'Welcome back',
    'allAnnouncements6'=>'Fasten your seat belts and',
    'allAnnouncements7'=>'accelerate ',
    'allAnnouncements8'=>'to the best offer!',
    'allAnnouncements9'=>'Market',
    'allAnnouncements10'=>'Start Selling',
    'ht' => 'TAKE A LOOK AT OUR TEAM',
    'ht1' => 'Minds behind Presto.it!',
    'ht2' => 'PEOPLE, JUST LIKE YOU!',
    'ht3' => 'Customers before ',
    'ht4' => 'developers!',
    'ht5' => '',
    'ht6' => 'Team',


    'wt' => 'WORK WITH US!',
    'wt1' => 'Join our team on Presto.it!',
    'wt2' => 'SUBMIT YOUR REQUEST',
    'wt3' => 'Manage the ',
    'wt4' => 'community',
    'wt5' => ', in Smart Working!',
    'wt6' => 'CLICK HERE',

];