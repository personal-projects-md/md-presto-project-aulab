<?php 

return [
    'ftText1' => 'It is a project that comes from the desire to create a revolutionary ',
    'ftTextH1' => ' e-commerce ',
    'ftT1s' => 'to give a ',
    'ftTextH2' => ' boost ',
    'ftText1t' => 'at the search of your favourite products.',
    'ftText2' => 'Always ',
    'ftTextH3' => ' safe',
    'ftTerms' => 'Terms & Conditions',
    'ftHelp' => 'Need Help?',
    'ftWho' => 'Team',
    'ftContact' => 'Contacts',
    'ftWork' => 'Work with us!',
];