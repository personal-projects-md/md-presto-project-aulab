<?php
return [
    'termstext' => 'Terms & Conditions',
    'terms' => "This document contains the general terms and conditions of use of the Presto.it website which offers e-commerce with the possibility of
    online trading provided by a third party supplier.
    
    - Definitions
    To allow a complete understanding and acceptance of these terms and conditions, the following terms, in the singular and al
    plural, will have the meaning indicated below.
    - Owner: Presto.it -BACKEND-STREET BOYS with registered office in Aulab, VAT number / Tax Code HACKADEMY46, H46, capital
    fully paid € 10.00, PEC address backendstreetboys@aulab.t
    Application: the Presto.it website
    
    The Owners do not assume any responsibility for any fraudulent or illegal use that may be tactful by third parties of the
    credit cards and other means of payment,
    The Owner will not be responsible for:
    any loss of commercial opportunity and any other loss, even indirect, possibly suffered by the User who
    they are not a direct consequence of the violation of the contract by the Owner
    incorrect or unsuitable use of the Application by Users or third parties
    In no case can the Owner be held responsible for a sum greater than double the cost paid by the user.
   
    - Major force
    The Owner cannot be held responsible for the failure or delayed fulfillment of its obligations, for
    Circumstances beyond the reasonable control of the tolare due to events of force majeure or, in any case, to unforeseen events
    and unpredictable and, in any case, independent of his will.
    The fulfillment of the obligations by the Owner will be considered suspended for the period in which torza events occur
    greater.
    The Owners will carry out any act in their power in order to identify solutions that allow the correct fulfillment of the
    their obligations despite the persistence of force majeure events.
    
    - Link to third party sites
    The Application may contain links to third party sites / applications. the Owner does not exercise any control over them and,
    therefore, it is not in any way responsible for the contents of this Yes / application.
    Some of these links may refer to third party sites / applications that provide services through the Application. In
    in these cases, the general conditions for the site / application and for the use of the service will apply to the individual services
    prepared by third parties, with respect to which the Owner assumes no responsibility.
    
    - Applicable law and competent court
    The Conditions are subject to Italian law.
    For Professional users, for any dispute relating to the Application, execution and interpretation of these Conditions
    the court of the place where the Data Controller is located is competent.
    For Consumer Users, any dispute relating to the application, execution and interpretation of these Conditions will be
    devolved to the forum of the place where the Consumer User resides or has elected domicile, if located in the territory of the Italian State,
    without prejudice to the right for the Consumer User to appeal to a judge other than that of the 'consumer forum' pursuant to art. 66 bis of the Code
    del Consumo, competent for the territory according to one of the criteria referred to in art. 18, 19 and 20 of the civil procedure code.
    And without prejudice to the application of the provisions to Consumer Users who do not have their habitual residence in Italy
    possibly more favorable and mandatory provided by the law of the country in which they have their habitual residence, in
    particularly in relation to the deadline for exercising the right of withdrawal, the deadline for returning the Products, in case of
    exercise of this right, the methods and formalities of its communication and the legal guarantee of conformity.
    14. Online Dispute Resolution for Consumer Users
    The Consumer User residing in Europe must be aware of the fact that the European Commission has set up a
    online platform that provides an alternative dispute resolution tool. Such a tool can be used
    by the Consumer User to resolve in a non-judicial way any dispute relating to it deriving from contracts for the sale of goods
    and supply of services stipulated on the network. Consequently, the Consumer User can use this platform for the resolution of any
    dispute arising from the contract stipulated online. The platform is available at the following address: ec.europa.eu/consumers/odr/
    Date 29/05/2022",

    'termstext2' => 'Privacy Policy',
    'privacy' => "The purpose of this document is to inform the natural person (hereinafter the 'Data Subject') regarding the processing of his / her data
    personal data (hereinafter 'Personal Data') collected by the data controller, ABC S.p.a, with registered office in Via Torino 1, 20100 Milan,
    CF / Partita VA 0123456789C, e-mail address company@email.it, PEC address company@pec.it, telephone 800 123 123, (hereinafter
    Owner), through the website www.example.it (hereinafter the 'Application').
    Changes and updates will be binding as soon as they are published on the Application. In case of non-acceptance of the
    changes made to the Privacy Policy, the interested party is required to cease using this Application and may request the Owner of
    delete their Personal Data.
    Categories of Personal Data processed
    The Data Controller processes the following types of Personal Data provided voluntarily by the interested party
    Contact details: name, surname, address, e-mail, telephone, images, authentication credentials, any additional ones
    information sent by the interested party, etc.
    Tax and payment data: tax code, VAT certificate, credit card details, bank account details, etc.",

    'termstext3' => 'Cookie Policy',
    'cookie' => "This website uses cookies
    We and third parties use cookies or similar technologies for technical functionality and, with your consent, also for other purposes described in the Cookie Policy such as collecting and processing personal data from devices, showing you personalized advertising, measuring its performance, analyzing our audiences and improving. our products and services. You can freely lend, refuse or withdraw your consent at any time by customizing your preferences. By clicking on the 'Accept all cookies' button you consent to the use of these technologies for all the purposes indicated. By clicking on the 'Accept technical cookies' button you consent to the use of technical cookies only.",

    'contacts' => 'GET IN TOUCH WITH US!',
];