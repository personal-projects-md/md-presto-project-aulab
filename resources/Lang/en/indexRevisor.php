<?php

return[
    'phrase1'=>'Presto.it - Revisor Tool',
    'phrase2'=>"What did users post while you were away?",
    'phrase3'=>'Congratulations! You have reviewed all the announcements!',
    'phrase4'=>'Review announcements',
    'phrase5'=>'You are the Presto.it fastest revisor!',
    'phrase6'=>'Published:',
    'phrase7'=>'By:',
    'phrase8'=>'Article description:',
    'phrase9'=>'Accept',
    'phrase10'=>'Reject',
    'phrase11'=>'Good job,',
    'phrase12'=>'New announcements are on the way for you to review!',
    'phrase13'=>'Market',
    'phrase14'=>'Home',

];