<?php

    return [
        'CatShoT0' => 'Presto.it - Browse by Category',
        'CatShoT1' => 'BROWSE BY CATEGORY!',
        'CatShoT2' => '',
        'CatShoT3' => 'Can we help you?',
        'CatShoT4' => 'Start Selling',
        'CatShoT5' => 'Filter by Category',
        'CatShoT6' => 'Tool of',
        'CatShoT7' => 'search',
        'CatShoT8' => ',at your service!',
        'CatShoT9' => 'Find out more',
        'CatShoT10' => 'There are no ads for this category...',
        'CatShoT11' => 'But you might be the first to enter one!',
        'CatShoT12' => 'It seems like this category is empty...',
        'CatShoT13' => 'Back'

    ];