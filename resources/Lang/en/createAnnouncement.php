<?php

    return [
        'CreAnn0' => 'Presto.it - Start Selling',
        'CreAnn1' => 'START SELLING',
        'CreAnn2' => 'What do you want to ',
        'CreAnn3' => 'sell',
        'CreAnn4' => 'today?',
        'CreAnn5' => 'Announcement Title',
        'CreAnn6' => 'Description',
        'CreAnn7' => 'Price',
        'CreAnn8' => 'Category',
        'CreAnn9' => 'Browse...',
        'CreAnn10' => 'Upload at least three images!',
        'CreAnn11' => 'We recommend the format',
        'CreAnn12' => '.png',
        'CreAnn13' => 'Preview',
        'CreAnn14' => 'of your images',
        'CreAnn15' => 'Remember that they will be',
        'CreAnn16' => 'resized',
        'CraAnn17' => 'automatically!',
        'CreAnn18' => 'Create',
        'annbtn' => 'Remove'
    ];