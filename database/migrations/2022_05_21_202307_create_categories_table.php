<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->string('name_it');
            $table->string('name_en');
            $table->string('name_es');

            $table->timestamps();
        });
        
        $categories = [ 
            ['Abbigliamento' , 'Clothing' , 'Ropa'],
            ['Arte', 'Art' , 'Arte'],
            ['Food & Wine' , 'Food & Wine' , 'Food & Wine'],
            ['Modellismo & Toys' , 'Models & Toys' , 'Modelismo y Juegos'],
            ['Immobili' , 'Properties' , 'Propriedades'],
            ['Informatica' , 'Tech' , 'Informatica'],
            ['Musica' , 'Music' , 'Música'],
            ['Motori' , 'Engines' , 'Motores'],
            ['Pet-Care' , 'Pet-care' , 'Pet-Care'],
            ['Sport' , 'Sport' , 'Sport'],
            ['Telefonia' , 'Mobile Phones' ,'Telefonía'],
            ['Viaggi' , 'Travel' , 'Excursiones' ],
            ['Videogames', 'Videogames' , 'Videogames'],
        ];
            foreach($categories as $category){

                Category::create([
                    'name_it' => $category[0],
                    'name_en' => $category[1],
                    'name_es' => $category[2],
                ]);
            }
        }
        
        /**
        * Reverse the migrations.
        *
        * @return void
        */
        public function down()
        {
            Schema::dropIfExists('categories');
        }
    };
    