<?php

use App\Models\Announcement;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'welcome'])->name('welcome');
Route::get('/categoria/{category}', [FrontController::class, 'categoryShow'])->name('categoryShow');

Route::get('/team', function(){Return view('team');})->name('team');
Route::get('/contacts', function(){Return view('contacts');})->name('contacts');
Route::get('/terms&conditions', function(){Return view('terms&conditions');})->name('terms&conditions');
Route::get('/privacy', function(){Return view('privacy');})->name('privacy');
Route::get('/cookie', function(){Return view('cookie');})->name('cookie');


/* ROTTE ANNUNCI */

Route::get('/nuovo/annuncio', [AnnouncementsController::class, 'createAnnouncement'])->middleware('auth')->name('announcements.create');

Route::get('/visualizza/annunci' , [AnnouncementsController::class, 'announcementShow'] )->name('announcements.show');


Route::get('/dettaglio/annuncio/{announcement}',[AnnouncementsController::class, 'detailAnnouncement'])->name('announcements.detail');

Route::get('tutti/annunci', [AnnouncementsController::class, 'indexAnnouncement'])->name('announcements.index');

/* FINE ROTTE ANNUNCI */

/* ROTTA HOME REVISORE */
Route::get('/revisore/home', [RevisorController::class , 'index' ])->middleware('isRevisor')->name('revisor.index');
// ROTTA FORM REVISOR
Route::get('form/revisor', function(){ return view('revisor.formRevisor');})->name('revisor.formRevisor');

/* ROTTA ACCETTA ANNUNCIO */
Route::patch('/accetta/annuncio/{announcement}' , [RevisorController::class , 'acceptAnnouncement'])->middleware('isRevisor')->name('revisor.accept_announcement');

/* ROTTA RIFIUTA ANNUNCIO */
Route::patch('/rifiuta/annuncio/{announcement}' , [RevisorController::class , 'rejectAnnouncement'])->middleware('isRevisor')->name('revisor.reject_announcement');

// richiesta revisore
Route::get('/richiesta/revisore', [RevisorController::class , 'becomeRevisor'])->middleware('auth')->name('become.revisor');
// rendi utente revisore
Route::get('/rendi/revisore/{user}', [RevisorController::class , 'makeRevisor'])->name('make.revisor');
// Ricerca annuncio
Route::get('/ricerca/annuncio' , [FrontController::class, 'searchAnnouncements'])->name('announcements.search');
//MODIFICA ANNUNCI

Route::get("/modifica/annuncio", [RevisorController::class, "updateRevisor"])->middleware('isRevisor')->name('revisor.update');
Route::patch('/annulla-revisione/{announcement}', [RevisorController::class,'nullRevisor'])->middleware('isRevisor')->name("revisor.null");



// Route lingue
Route::post('/lingua/{lang}', [FrontController::class, 'setLanguage'])->name('set_language_locale');